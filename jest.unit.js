const baseConfig = require("./jest.config");

module.exports = {
  ...baseConfig,
  testMatch: ["**/?(*.)+(spec).js"],
  collectCoverageFrom: [
    "!**/?(*.)+(spec|test|faker).js",
    "!**/?(*.)+(schema|model|models).js",
    "!**/(__tests__|config|bin|constants|routes|HTMLParser)/**/*",
    "!**/(index|constants).js",
    "!**/*server.js",
    "!coverage/**/*",
    "!jest.*",
    "!.*",
    "^src/**/*.js"
  ],
  collectCoverage: true
};
