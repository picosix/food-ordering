# food-ordering

> Home test: Food Ordering System

## Validation steps

### Notice

Instead of create interaction test, I described test cases step-by-step by `curl` commands. It makes sense. If you want to see how I described interaction test, please see on folder `__tests__`.

### Limitation

- The ACL system is not dynamic. I just defined it on `config/auth.js`

- Price and cooking ​duration cannot be search by range. As same as other fields with type `Number` and `Date` of all schemas. Beucase they were not a strict required, I left it for improvement process

- Some static functions of schemas was not tested.

- I didn't implement logout logic. Just remove access token on client.

- I also didn't implement logic of `Refresh Tokens`. Just set access token expired time is `365d`

### Prepare

- Copy `.env.example` to `.env` and set appropriate value to environment variables

- Seed the first Administrator to mongo

  ```json
  {
    "username": "gloria_abshire53",
    "password": "$2b$10$/woenXJAn4srpqJv/UXwS..eCxrFr7OfoJP/uR2jwsB50QDrhcg0e", // 123456
    "name": {
      "first": "Diego",
      "last": "Kunde",
      "middle": "Lavonne Hagenes"
    },
    "roleId": "ADMINISTRATOR"
  }
  ```

- Get access token

  ```shell
  $ curl -d '{"username":"gloria_abshire53", "password":"123456"}' -H "Content-Type: application/json" -X POST http://localhost:9001/auth/local
  {"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"}
  ```

- Start development REST server

  ```shell
  yarn dev:www
  ```

### Test Requirements

1. There is a content management system for the administrator​ t​ o list/create/edit/delete dishes​ comprising of ​description, image, price ​and cooking ​duration​.

   - Create new dish

     ```shell
     $ curl -d '{"name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"portions":["hic","omnis","iusto"]}' -H "Content-Type: application/json" -X POST "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

     {"data":{"portions":["hic","omnis","iusto"],"_id":"5c3aecacebbc4918c5285af6","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"createdAt":"2019-01-13T07:45:48.379Z","updatedAt":"2019-01-13T07:45:48.379Z","__v":0}}
     ```

   - Create another dish

     ```shell
     $ curl -d '{"name":"reprehenderit","description":"Quas molestias velit id aspernatur ipsum quo.","image":"http://lorempixel.com/640/480/nightlife","price":90501,"duration":41991,"portions":["atque","dolorem","illum"]}' -H "Content-Type: application/json" -X POST "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

     {"data":{"portions":["atque","dolorem","illum"],"_id":"5c3aed5ba4e4441a860e8ec4","name":"reprehenderit","description":"Quas molestias velit id aspernatur ipsum quo.","image":"http://lorempixel.com/640/480/nightlife","price":90501,"duration":41991,"createdAt":"2019-01-13T07:48:43.433Z","updatedAt":"2019-01-13T07:48:43.433Z","__v":0}}
     ```

   - Get lists of dishes

     ```shell
     $ curl -H "Content-Type: application/json" "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

     {"total":2,"data":[{"portions":["hic","omnis","iusto"],"_id":"5c3aecacebbc4918c5285af6","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"createdAt":"2019-01-13T07:45:48.379Z","updatedAt":"2019-01-13T07:45:48.379Z","__v":0},{"portions":["atque","dolorem","illum"],"_id":"5c3aed5ba4e4441a860e8ec4","name":"reprehenderit","description":"Quas molestias velit id aspernatur ipsum quo.","image":"http://lorempixel.com/640/480/nightlife","price":90501,"duration":41991,"createdAt":"2019-01-13T07:48:43.433Z","updatedAt":"2019-01-13T07:48:43.433Z","__v":0}]}%
     ```

   - Update dish. You MUST use your responsed id on your process. (With me, it is the second id `5c3aed5ba4e4441a860e8ec4`)

     ```shell
     $ curl -d '{"price":19999,"duration":21000}' -H "Content-Type: application/json" -H "X-HTTP-Method-Override: PATCH" -X POST "http://localhost:9001/dishes/5c3aed5ba4e4441a860e8ec4?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

     {"data":{"_id":"5c3aed5ba4e4441a860e8ec4","portions":["atque","dolorem","illum"],"name":"reprehenderit","description":"Quas molestias velit id aspernatur ipsum quo.","image":"http://lorempixel.com/640/480/nightlife","price":19999,"duration":21000,"createdAt":"2019-01-13T07:48:43.433Z","updatedAt":"2019-01-13T07:54:55.240Z","__v":0}}
     ```

   - Delete dish

     ```shell
     $ curl -H "Content-Type: application/json" -H "X-HTTP-Method-Override: DELETE" -X POST "http://localhost:9001/dishes/5c3aed5ba4e4441a860e8ec4?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

     {"data":{"_id":"5c3aed5ba4e4441a860e8ec4","name":"reprehenderit"}}
     ```

     - Prepare for next test case, we should add 4 dishes

     ```shell
     $ curl -H "Content-Type: application/json" -d '@data/dish_consequatur.json' -X POST "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c" && \
     curl -H "Content-Type: application/json" -d '@data/dish_fugit.json' -X POST "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c" && \
     curl -H "Content-Type: application/json" -d '@data/dish_iusto.json' -X POST "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c" && \
     curl -H "Content-Type: application/json" -d '@data/dish_voluptatibus.json' -X POST "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

     {"data":{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3af77c0000d02fbe438b3b","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"createdAt":"2019-01-13T08:31:56.129Z","updatedAt":"2019-01-13T08:31:56.129Z","__v":0}}
     {"data":{"portions":["magni","voluptas","tempore"],"_id":"5c3af77c0000d02fbe438b3c","name":"fugit","description":"Cum dolore atque occaecati fuga itaque non.","image":"http://lorempixel.com/640/480/sports","price":70349,"duration":97695,"createdAt":"2019-01-13T08:31:56.166Z","updatedAt":"2019-01-13T08:31:56.166Z","__v":0}}
     {"data":{"portions":["at","consequatur","dolor"],"_id":"5c3af77c0000d02fbe438b3d","name":"iusto","description":"Debitis quos quo.","image":"http://lorempixel.com/640/480/nature","price":95202,"duration":36835,"createdAt":"2019-01-13T08:31:56.194Z","updatedAt":"2019-01-13T08:31:56.194Z","__v":0}}
     {"data":{"portions":["sed","harum","suscipit"],"_id":"5c3af77c0000d02fbe438b3e","name":"voluptatibus","description":"Dolor sit quam quasi culpa dicta et natus pariatur doloremque.","image":"http://lorempixel.com/640/480/animals","price":42693,"duration":303,"createdAt":"2019-01-13T08:31:56.220Z","updatedAt":"2019-01-13T08:31:56.220Z","__v":0}}
     ```

2. Customer ​can signup, signin, signout

   - Register new customer

     ```shell
     $ curl -d '{"name":{"first":"Gabriella","last":"Abbott","middle":"Domenic Wiza"},"username":"wayne66","password":"123456","confirmPassword":"123456"}' -H "Content-Type: application/json" -X POST "http://localhost:9001/auth/local/register"

     {"data":{"username":"wayne66"}}
     ```

   - Login with registered account

     ```shell
     $ curl -d '{"username":"wayne66", "password":"123456"}' -H "Content-Type: application/json" -X POST http://localhost:9001/auth/local
     {"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoid2F5bmU2NiJ9LCJpYXQiOjE1NDczNzQ0OTIsImV4cCI6MTU3ODkxMDQ5Mn0.p79CCaFmQ6JwR4kQk5bFtK49NCWHHgyoCs6QaLlgQYg"}
     ```

3. To accommodate ​customers’​ mobile application, these APIs are needed

   - API to list all the available ​dishes

     ```shell
     $ curl -H "Content-Type: application/json" "http://localhost:9001/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoid2F5bmU2NiJ9LCJpYXQiOjE1NDczNzQ0OTIsImV4cCI6MTU3ODkxMDQ5Mn0.p79CCaFmQ6JwR4kQk5bFtK49NCWHHgyoCs6QaLlgQYg"

     {"total":5,"data":[{"portions":["hic","omnis","iusto"],"_id":"5c3aecacebbc4918c5285af6","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"createdAt":"2019-01-13T07:45:48.379Z","updatedAt":"2019-01-13T07:45:48.379Z","__v":0},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3af77c0000d02fbe438b3b","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"createdAt":"2019-01-13T08:31:56.129Z","updatedAt":"2019-01-13T08:31:56.129Z","__v":0},{"portions":["magni","voluptas","tempore"],"_id":"5c3af77c0000d02fbe438b3c","name":"fugit","description":"Cum dolore atque occaecati fuga itaque non.","image":"http://lorempixel.com/640/480/sports","price":70349,"duration":97695,"createdAt":"2019-01-13T08:31:56.166Z","updatedAt":"2019-01-13T08:31:56.166Z","__v":0},{"portions":["at","consequatur","dolor"],"_id":"5c3af77c0000d02fbe438b3d","name":"iusto","description":"Debitis quos quo.","image":"http://lorempixel.com/640/480/nature","price":95202,"duration":36835,"createdAt":"2019-01-13T08:31:56.194Z","updatedAt":"2019-01-13T08:31:56.194Z","__v":0},{"portions":["sed","harum","suscipit"],"_id":"5c3af77c0000d02fbe438b3e","name":"voluptatibus","description":"Dolor sit quam quasi culpa dicta et natus pariatur doloremque.","image":"http://lorempixel.com/640/480/animals","price":42693,"duration":303,"createdAt":"2019-01-13T08:31:56.220Z","updatedAt":"2019-01-13T08:31:56.220Z","__v":0}]}
     ```

   - API to submit an ​order ​of ​dishes ​(and its number of ​portions​)

     - Find temporary order for customer

       ```shell
       $ curl -H "Content-Type: application/json" -X POST "http://localhost:9001/orders/temp?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoid2F5bmU2NiJ9LCJpYXQiOjE1NDczNzQ0OTIsImV4cCI6MTU3ODkxMDQ5Mn0.p79CCaFmQ6JwR4kQk5bFtK49NCWHHgyoCs6QaLlgQYg"

       {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[],"createdAt":"2019-01-13T08:56:10.973Z","updatedAt":"2019-01-13T08:56:10.973Z","__v":0,"details":{"price":0,"duration":0,"waitingTime":0},"id":"5c3afd2a11193337ce3140ef"}}
       ```

     - Add dishes to order

       ```shell
       $ curl -d '{"ids":["5c3aecacebbc4918c5285af6","5c3af77c0000d02fbe438b3b"]}' -H "Content-Type: application/json" -X POST "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoid2F5bmU2NiJ9LCJpYXQiOjE1NDczNzQ0OTIsImV4cCI6MTU3ODkxMDQ5Mn0.p79CCaFmQ6JwR4kQk5bFtK49NCWHHgyoCs6QaLlgQYg"

       {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T09:00:54.661Z","__v":0}}
       ```

     - Customer ​can see the total ​price ​and ​waiting time a​ fter ordering

       ```shell
       $ curl -H "Content-Type: application/json" "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoid2F5bmU2NiJ9LCJpYXQiOjE1NDczNzQ0OTIsImV4cCI6MTU3ODkxMDQ5Mn0.p79CCaFmQ6JwR4kQk5bFtK49NCWHHgyoCs6QaLlgQYg"

       {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T09:05:36.404Z","__v":0,"details":{"price":77939,"duration":127296,"waitingTime":127296},"id":"5c3afe398fb6383a1c3cd95c"}}
       ```

     - Place order. **Notiec** `placedAt` fields just a flag, an original placed order time will be calculated by server

       ```shell
       $ curl -d '{"placedAt":"2019-01-13T09:04:58.396Z"}' -H "Content-Type: application/json" -H "X-HTTP-Method-Override: PATCH"  -X POST "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

       {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T09:05:36.404Z","__v":0,"placedAt":"2019-01-13T09:05:36.402Z"}}
       ```

4. Administrator ​can login, logout and create/delete other ​administrator or chef

   - Create new ADMINISTRATOR

   ```shell
   $ curl -d '{"username":"newton_lebsack","password":"123456","confirmPassword":"123456","name":{"first":"Zula","last":"Lockman","middle":"Aniyah Hane"},"roleId":"ADMINISTRATOR"}' -H "Content-Type: application/json" -X POST "http://localhost:9001/users?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

   {"data":{"username":"newton_lebsack"}}
   ```

   - Create new CHEF

   ```shell
   $ curl -d '{"username":"jamarcus27","password":"123456","confirmPassword":"123456","roleId":"CHEF","name":{"first":"Cameron","last":"Hegmann","middle":"Ernestine Batz"}}' -H "Content-Type: application/json" -X POST "http://localhost:9001/users?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

   {"data":{"username":"jamarcus27"}}
   ```

   - Delete an ADMINISTRATOR

   ```shell
   $ curl -H "Content-Type: application/json" -H "X-HTTP-Method-Override: DELETE" -X POST "http://localhost:9001/users/newton_lebsack?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiZ2xvcmlhX2Fic2hpcmU1MyJ9LCJpYXQiOjE1NDczNjQ4NTEsImV4cCI6MTU3ODkwMDg1MX0.kf0FtZyh9kaG5MKcrJXxUhiiT33xmCt1FQqjJmFQ1-c"

   {"data":{"username":"newton_lebsack"}}
   ```

5. To accommodate ​chefs’​ mobile application, an API is needed

   - Login by chef

     ```shell
     $ curl -d '{"username":"jamarcus27", "password":"123456"}' -H "Content-Type: application/json" -X POST http://localhost:9001/auth/local
     {"accessToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiamFtYXJjdXMyNyJ9LCJpYXQiOjE1NDczNzM3ODMsImV4cCI6MTU3ODkwOTc4M30.bPYW6NEj3YPyQlGIrC_7gTj_Shv1f5Vn9J_7TZTg5Ho"}
     ```

   - API to get the next ​dish ​(and the number of ​portions​)​ ​to be cooked. **Notice** That API will return orders with non-completed cook dishes

     ```shell
     $ curl -H "Content-Type: application/json" "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiamFtYXJjdXMyNyJ9LCJpYXQiOjE1NDczNzM3ODMsImV4cCI6MTU3ODkwOTc4M30.bPYW6NEj3YPyQlGIrC_7gTj_Shv1f5Vn9J_7TZTg5Ho"

     {"data":[{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T09:05:36.404Z","__v":0,"placedAt":"2019-01-13T09:05:36.402Z","details":{"price":77939,"duration":127296,"waitingTime":127296},"id":"5c3afe398fb6383a1c3cd95c"}]}
     ```

   - Mark a dish of order done

     ```shell
     $ curl -d '{"completedAt":"2019-01-13T10:08:06.627Z"}' -H "Content-Type: application/json" -H "X-HTTP-Method-Override: PATCH" -X POST "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c/dishes/5c3afe468fb6383a1c3cd95e?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiamFtYXJjdXMyNyJ9LCJpYXQiOjE1NDczNzM3ODMsImV4cCI6MTU3ODkwOTc4M30.bPYW6NEj3YPyQlGIrC_7gTj_Shv1f5Vn9J_7TZTg5Ho"

     {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T10:10:08.763Z","createdAt":"2019-01-13T09:00:54.661Z","completedAt":"2019-01-13T10:10:08.761Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T10:10:08.763Z","__v":0,"placedAt":"2019-01-13T09:05:36.402Z"}}
     ```

   - **Customer ​can see the approximate ​waiting time ​before ordering**

     ```shell
      curl -H "Content-Type: application/json" "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoid2F5bmU2NiJ9LCJpYXQiOjE1NDczNzQ0OTIsImV4cCI6MTU3ODkxMDQ5Mn0.p79CCaFmQ6JwR4kQk5bFtK49NCWHHgyoCs6QaLlgQYg"

      {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T10:10:08.763Z","createdAt":"2019-01-13T09:00:54.661Z","completedAt":"2019-01-13T10:10:08.761Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T10:10:08.763Z","__v":0,"placedAt":"2019-01-13T09:05:36.402Z","details":{"price":77939,"duration":127296,"waitingTime":56196},"id":"5c3afe398fb6383a1c3cd95c"}}
     ```

   - Get orders and dishes to cook again

     ```shell
     curl -H "Content-Type: application/json" "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c/dishes?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiamFtYXJjdXMyNyJ9LCJpYXQiOjE1NDczNzM3ODMsImV4cCI6MTU3ODkwOTc4M30.bPYW6NEj3YPyQlGIrC_7gTj_Shv1f5Vn9J_7TZTg5Ho"

     {"data":[{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T10:10:08.763Z","createdAt":"2019-01-13T09:00:54.661Z","completedAt":"2019-01-13T10:10:08.761Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T09:00:54.661Z","createdAt":"2019-01-13T09:00:54.661Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T10:10:08.763Z","__v":0,"placedAt":"2019-01-13T09:05:36.402Z","details":{"price":77939,"duration":127296,"waitingTime":56196},"id":"5c3afe398fb6383a1c3cd95c"}]}
     ```

   - Mark remain dishes of order done

     ```shell
     $ curl -d '{"completedAt":"2019-01-13T10:08:06.627Z"}' -H "Content-Type: application/json" -H "X-HTTP-Method-Override: PATCH" -X POST "http://localhost:9001/orders/5c3afe398fb6383a1c3cd95c/dishes/5c3afe468fb6383a1c3cd95d?accessToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJuYW1lIjoiamFtYXJjdXMyNyJ9LCJpYXQiOjE1NDczNzM3ODMsImV4cCI6MTU3ODkwOTc4M30.bPYW6NEj3YPyQlGIrC_7gTj_Shv1f5Vn9J_7TZTg5Ho"

     {"data":{"_id":"5c3afe398fb6383a1c3cd95c","customerId":"5c3ae85407faef0fb02eb01e","dishes":[{"portions":["hic","omnis","iusto"],"_id":"5c3afe468fb6383a1c3cd95e","name":"eaque","description":"Autem nisi praesentium non quia non.","image":"http://lorempixel.com/640/480/transport","price":19791,"duration":71100,"updatedAt":"2019-01-13T10:10:08.763Z","createdAt":"2019-01-13T09:00:54.661Z","completedAt":"2019-01-13T10:10:08.761Z"},{"portions":["adipisci","voluptatem","aliquam"],"_id":"5c3afe468fb6383a1c3cd95d","name":"consequatur","description":"Adipisci qui et similique.","image":"http://lorempixel.com/640/480/sports","price":58148,"duration":56196,"updatedAt":"2019-01-13T10:17:30.657Z","createdAt":"2019-01-13T09:00:54.661Z","completedAt":"2019-01-13T10:17:30.657Z"}],"createdAt":"2019-01-13T09:00:41.005Z","updatedAt":"2019-01-13T10:17:30.657Z","__v":0,"placedAt":"2019-01-13T09:05:36.402Z"}}
     ```

## Quick Started

- Copy `.env.example` to `.env` and set appropriate value to environment variables

- Start project by command `yarn pm2`

## Development

- Copy `.env.example` to `.env` and set appropriate value to environment variables

- Start project by command `yarn dev:www`

## Test

- Copy `.env.example` to `.env` and set appropriate value to environment variables

- Run unit tests by command `yarn test:unit`

- Run interactions tests by command `yarn test:interactions`
