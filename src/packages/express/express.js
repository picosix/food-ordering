const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const compression = require("compression");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");

const rateLimiter = require("./middleware/rateLimiter");
const restQueryParser = require("./middleware/restQueryParser");

module.exports = opts => {
  const server = express();
  server.use(cors());
  server.use(methodOverride());
  server.use(helmet());
  server.use(compression());
  server.use(bodyParser.json());

  server.use(rateLimiter(opts.rateLimiter));
  server.use(restQueryParser(opts.restQueryParser));

  if (opts.settings) server.set("settings", opts.settings);
  if (opts.logger) server.set("logger", opts.logger);
  if (opts.models) server.set("models", opts.models);

  server.set("info", {
    name: process.env.npm_package_name,
    version: process.env.npm_package_version,
    description: process.env.npm_package_description,
    license: process.env.npm_package_license,
    author: process.env.npm_package_author_name
  });

  return server;
};
