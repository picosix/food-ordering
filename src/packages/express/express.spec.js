const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const compression = require("compression");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");

const rateLimiter = require("./middleware/rateLimiter");
const restQueryParser = require("./middleware/restQueryParser");

const server = require("./express");

const fakeExpressServer = {
  use: jest.fn(() => fakeExpressServer),
  set: jest.fn(() => fakeExpressServer)
};

jest.mock("express");
jest.mock("cors");
jest.mock("helmet");
jest.mock("compression");
jest.mock("body-parser");
jest.mock("method-override");
jest.mock("./middleware/rateLimiter");
jest.mock("./middleware/restQueryParser");

describe("express", () => {
  beforeAll(() => {
    express.mockImplementation(() => fakeExpressServer);
    bodyParser.json = jest.fn();
  });

  beforeEach(() => {
    express.mockClear();
    fakeExpressServer.use.mockClear();
    fakeExpressServer.set.mockClear();

    cors.mockClear();
    helmet.mockClear();
    compression.mockClear();
    bodyParser.json.mockClear();
    methodOverride.mockClear();
    rateLimiter.mockClear();
    restQueryParser.mockClear();
  });

  it("should setup express server with DEFAULT options", () => {
    cors.mockReturnValueOnce("cors");
    helmet.mockReturnValueOnce("helmet");
    compression.mockReturnValueOnce("compression");
    bodyParser.json.mockReturnValueOnce("bodyParser.json");
    methodOverride.mockReturnValueOnce("methodOverride");
    rateLimiter.mockReturnValueOnce("rateLimiter");
    restQueryParser.mockReturnValueOnce("restQueryParser");

    const opts = {
      rateLimiter: {
        redis: { host: "localhost", port: 6379, db: 1 },
        config: { points: 1000, duration: 1 }
      },
      restQueryParser: { maxLimit: 100, defaultLimit: 20, maxPage: 10 }
    };

    server(opts);

    expect(express).toHaveBeenCalledTimes(1);

    expect(fakeExpressServer.use).toHaveBeenCalledTimes(7);
    expect(fakeExpressServer.use.mock.calls[0][0]).toBe("cors");
    expect(fakeExpressServer.use.mock.calls[1][0]).toBe("methodOverride");
    expect(fakeExpressServer.use.mock.calls[2][0]).toBe("helmet");
    expect(fakeExpressServer.use.mock.calls[3][0]).toBe("compression");
    expect(fakeExpressServer.use.mock.calls[4][0]).toBe("bodyParser.json");
    expect(fakeExpressServer.use.mock.calls[5][0]).toBe("rateLimiter");
    expect(fakeExpressServer.use.mock.calls[6][0]).toBe("restQueryParser");

    expect(fakeExpressServer.set).toHaveBeenCalledTimes(1);
  });

  it("should setup express server with options", () => {
    cors.mockReturnValueOnce("cors");
    helmet.mockReturnValueOnce("helmet");
    compression.mockReturnValueOnce("compression");
    bodyParser.json.mockReturnValueOnce("bodyParser.json");
    methodOverride.mockReturnValueOnce("methodOverride");
    rateLimiter.mockReturnValueOnce("rateLimiter");
    restQueryParser.mockReturnValueOnce("restQueryParser");

    const opts = {
      rateLimiter: {
        redis: { host: "localhost", port: 6379, db: 1 },
        config: { points: 1000, duration: 1 }
      },
      restQueryParser: { maxLimit: 100, defaultLimit: 20, maxPage: 10 },
      settings: {
        auth: { secretKey: "123abc456xyz", expiresIn: "365d", saltRounds: 10 }
      },
      logger: console,
      models: { Users: {} }
    };

    server(opts);

    expect(express).toHaveBeenCalledTimes(1);

    expect(fakeExpressServer.use).toHaveBeenCalledTimes(7);
    expect(fakeExpressServer.use.mock.calls[0][0]).toBe("cors");
    expect(fakeExpressServer.use.mock.calls[1][0]).toBe("methodOverride");
    expect(fakeExpressServer.use.mock.calls[2][0]).toBe("helmet");
    expect(fakeExpressServer.use.mock.calls[3][0]).toBe("compression");
    expect(fakeExpressServer.use.mock.calls[4][0]).toBe("bodyParser.json");
    expect(fakeExpressServer.use.mock.calls[5][0]).toBe("rateLimiter");
    expect(fakeExpressServer.use.mock.calls[6][0]).toBe("restQueryParser");

    expect(fakeExpressServer.set).toHaveBeenCalledTimes(4);
    expect(fakeExpressServer.set.mock.calls[0][0]).toBe(
      "settings",
      opts.settings
    );
    expect(fakeExpressServer.set.mock.calls[1][0]).toBe("logger", opts.logger);
    expect(fakeExpressServer.set.mock.calls[2][0]).toBe("models", opts.models);
  });
});
