# rateLimiter

## Quick Started

```js
...

const rateLimiter = require("./middleware/rateLimiter");

...

// Default options
const rateLimiterOpts = {
  points: Number(process.env.DEFAULT_RATE_LIMIT_CONFIG_POINTS) || 1000,
  duration: Number(process.env.DEFAULT_RATE_LIMIT_CONFIG_DURATION) || 1
};
server.use(rateLimiter(rateLimiterOpts));
```
