const Redis = require("ioredis");
const { RateLimiterRedis } = require("rate-limiter-flexible");

const rateLimiter = require("./rateLimiter");

jest.mock("ioredis");
jest.mock("rate-limiter-flexible");

const redis = { host: "localhost", port: 6379, db: 1 };
const res = {
  json: jest.fn(),
  status: jest.fn(() => res)
};
const next = jest.fn();

const storeClient = {};
Redis.mockImplementation(() => storeClient);

const fakeRateLimiterRedis = {
  consume: jest.fn()
};
RateLimiterRedis.mockImplementation(() => fakeRateLimiterRedis);

describe("apps.api.middleware.rateLimiter", () => {
  beforeEach(() => {
    Redis.mockClear();
    RateLimiterRedis.mockClear();
    fakeRateLimiterRedis.consume.mockClear();

    res.json.mockClear();
    res.status.mockClear();
    next.mockClear();
  });

  it("should allow connect with default options", async () => {
    let req = { connection: { remoteAddress: "192.168.1.1" } };

    fakeRateLimiterRedis.consume.mockResolvedValueOnce(true);

    await rateLimiter({ redis })(req, res, next);

    expect(Redis).toHaveBeenCalledTimes(1);
    expect(Redis).toHaveBeenCalledWith({ ...redis, enableOfflineQueue: false });

    expect(RateLimiterRedis).toHaveBeenCalledTimes(1);
    expect(RateLimiterRedis).toHaveBeenCalledWith({
      storeClient,
      points: 1000,
      duration: 1
    });

    expect(fakeRateLimiterRedis.consume).toHaveBeenCalledTimes(1);
    expect(fakeRateLimiterRedis.consume).toHaveBeenCalledWith(
      req.connection.remoteAddress
    );

    expect(next).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();
    expect(res.json).not.toBeCalled();
  });

  it("should throw too many requests error", async () => {
    let req = { connection: { remoteAddress: "192.168.1.1" } };

    fakeRateLimiterRedis.consume.mockRejectedValueOnce({
      _remainingPoints: 0,
      _msBeforeNext: 454,
      _consumedPoints: 12,
      _isFirstInDuration: false
    });

    await rateLimiter({ redis })(req, res, next);

    expect(Redis).toHaveBeenCalledTimes(1);
    expect(Redis).toHaveBeenCalledWith({
      ...redis,
      enableOfflineQueue: false
    });

    expect(RateLimiterRedis).toHaveBeenCalledTimes(1);
    expect(RateLimiterRedis).toHaveBeenCalledWith({
      storeClient,
      points: 1000,
      duration: 1
    });

    expect(fakeRateLimiterRedis.consume).toHaveBeenCalledTimes(1);
    expect(fakeRateLimiterRedis.consume).toHaveBeenCalledWith(
      req.connection.remoteAddress
    );

    expect(next).not.toBeCalled();

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(429);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: "Too Many Requests" });
  });
});
