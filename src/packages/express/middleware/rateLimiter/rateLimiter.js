const Redis = require("ioredis");
const { RateLimiterRedis } = require("rate-limiter-flexible");

const DEFAULT_CONNECTION = {
  host:
    /* istanbul ignore next */ process.env.DEFAULT_REDIS_HOST || "localhost",
  port: /* istanbul ignore next */ process.env.DEFAULT_REDIS_PORT || 6379
};
const DEFAULT_RATE_LIMIT_CONFIG = {
  points: Number(process.env.DEFAULT_RATE_LIMIT_CONFIG_POINTS) || 1000,
  duration: Number(process.env.DEFAULT_RATE_LIMIT_CONFIG_DURATION) || 1
};

const rateLimiter = opts => {
  let { redis, config } = opts;

  const storeClient = new Redis({
    ...DEFAULT_CONNECTION,
    ...redis,
    enableOfflineQueue: false
  });
  const rateLimiter = new RateLimiterRedis({
    storeClient,
    ...DEFAULT_RATE_LIMIT_CONFIG,
    ...config
  });

  return (req, res, next) =>
    rateLimiter
      .consume(req.connection.remoteAddress)
      .then(() => next())
      .catch(() => res.status(429).json({ message: "Too Many Requests" }));
};

module.exports = rateLimiter;
module.exports.DEFAULT_RATE_LIMIT_CONFIG = DEFAULT_RATE_LIMIT_CONFIG;
