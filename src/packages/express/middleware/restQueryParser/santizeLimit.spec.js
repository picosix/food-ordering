const santizeLimit = require("./santizeLimit");

const maxLimit = 100;
const defaultLimit = 20;

describe("apps.api.middleware.restQueryParser.santizeLimit", () => {
  it("should return default limit if request limit is not a valid positive integer", () => {
    expect(santizeLimit({ maxLimit, defaultLimit, limit: "#!@^@&!" })).toBe(
      defaultLimit
    );
    expect(santizeLimit({ maxLimit, defaultLimit, limit: 0 })).toBe(
      defaultLimit
    );
    expect(santizeLimit({ maxLimit, defaultLimit, limit: -1 })).toBe(
      defaultLimit
    );
  });

  it("should return max limit if request limit is greater than it", () => {
    expect(
      santizeLimit({
        maxLimit,
        defaultLimit,
        limit: maxLimit + 1
      })
    ).toBe(maxLimit);
  });

  it("should return request limit for other case", () => {
    const limit = 15;
    expect(
      santizeLimit({
        maxLimit,
        defaultLimit,
        limit
      })
    ).toBe(limit);
  });
});
