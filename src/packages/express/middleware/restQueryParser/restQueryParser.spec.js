const restQueryParser = require("./restQueryParser");
const santizeLimit = require("./santizeLimit");
const santizeSkip = require("./santizeSkip");
const santizeSort = require("./santizeSort");

jest.mock("./santizeLimit");
jest.mock("./santizeSkip");
jest.mock("./santizeSort");

const apiConfig = {
  maxLimit: 100,
  defaultLimit: 20,
  maxPage: 10
};
const fakeApp = {
  get: jest.fn(() => apiConfig)
};
const next = jest.fn();

describe("apps.api.middleware.restQueryParser.restQueryParser", () => {
  beforeEach(() => {
    santizeLimit.mockClear();
    santizeSkip.mockClear();
    santizeSort.mockClear();

    next.mockClear();
  });

  it("should move to next route if request method is not GET", () => {
    let req = { method: "POST" };
    restQueryParser(apiConfig)(req, {}, next);

    expect(next).toHaveBeenCalledTimes(1);

    expect(santizeLimit).not.toBeCalled();
    expect(santizeSkip).not.toBeCalled();
    expect(santizeSort).not.toBeCalled();
  });

  it("should calculate paging properties then move to next route", () => {
    let query = { _limit: 15, _page: 2, _sort: "-createdAt" };
    let req = { app: fakeApp, method: "GET", query };
    santizeLimit.mockReturnValueOnce(query._limit);

    restQueryParser(apiConfig)(req, {}, next);

    expect(santizeLimit).toHaveBeenCalledTimes(1);
    expect(santizeLimit).toHaveBeenCalledWith({
      maxLimit: apiConfig.maxLimit,
      defaultLimit: apiConfig.defaultLimit,
      limit: query._limit
    });

    expect(santizeSkip).toHaveBeenCalledTimes(1);
    expect(santizeSkip).toHaveBeenCalledWith({
      maxPage: apiConfig.maxPage,
      page: query._page,
      limit: query._limit
    });

    expect(santizeSort).toHaveBeenCalledTimes(1);
    expect(santizeSort).toHaveBeenCalledWith(query._sort);

    expect(next).toHaveBeenCalledTimes(1);
  });
});
