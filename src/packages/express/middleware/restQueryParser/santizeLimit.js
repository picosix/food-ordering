module.exports = ({ maxLimit, defaultLimit, limit }) => {
  limit = Number(limit);
  if (!Number.isInteger(limit) || limit < 1) {
    limit = defaultLimit;
  }
  return limit > maxLimit ? maxLimit : limit;
};
