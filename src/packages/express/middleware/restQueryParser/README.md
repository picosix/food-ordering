# restQueryParser

## Quick Started

```js
...

const restQueryParser = require("./middleware/restQueryParser");

...

// Default options
const restQueryParserOpts = {
  maxLimit: Number(process.env.DEFAULT_REST_QUERY_COFNIG_MAX_LIMIT) || 100,
  defaultLimit:
    Number(process.env.DEFAULT_REST_QUERY_COFNIG_DEFAULT_LIMIT) || 20,
  maxPage: Number(process.env.DEFAULT_REST_QUERY_COFNIG_MAX_PAGE) || 10
};
server.use(restQueryParser(restQueryParserOpts));
```
