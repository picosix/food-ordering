const santizeSort = require("./santizeSort");

describe("apps.api.middleware.restQueryParser.santizeSort", () => {
  it("should return empty object if sort param is not a valid string", () => {
    expect(santizeSort()).toEqual({});
    expect(santizeSort("")).toEqual({});
  });

  it("should return sort object with rule [- => DESC, + => ASC]", () => {
    expect(santizeSort("-name,updatedAt")).toEqual({ name: -1, updatedAt: 1 });
  });

  it("should return empty object because of invalid sort", () => {
    expect(santizeSort("-, -, -")).toEqual({});
  });
});
