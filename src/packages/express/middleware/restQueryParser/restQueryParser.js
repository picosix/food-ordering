const santizeLimit = require("./santizeLimit");
const santizeSkip = require("./santizeSkip");
const santizeSort = require("./santizeSort");

const DEFAULT_REST_QUERY_COFNIG = {
  maxLimit:
    /* istanbul ignore next */ Number(
      process.env.DEFAULT_REST_QUERY_COFNIG_MAX_LIMIT
    ) || 100,
  defaultLimit:
    /* istanbul ignore next */ Number(
      process.env.DEFAULT_REST_QUERY_COFNIG_DEFAULT_LIMIT
    ) || 20,
  maxPage:
    /* istanbul ignore next */ Number(
      process.env.DEFAULT_REST_QUERY_COFNIG_MAX_PAGE
    ) || 10
};

const restQueryParser = opts => (req, res, next) => {
  if (req.method !== "GET") return next();

  let { maxLimit, defaultLimit, maxPage } = {
    ...DEFAULT_REST_QUERY_COFNIG,
    ...opts
  };
  let { _limit: limit, _page: page, _sort: sort } = req.query;

  limit = santizeLimit({ maxLimit, defaultLimit, limit });
  let skip = santizeSkip({ maxPage, page, limit });
  sort = santizeSort(sort);

  req._paging = { limit, skip, sort };
  return next();
};

module.exports = restQueryParser;
module.exports.DEFAULT_REST_QUERY_COFNIG = DEFAULT_REST_QUERY_COFNIG;
