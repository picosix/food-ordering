const santizeSkip = require("./santizeSkip");

const maxPage = 10;
const limit = 20;

describe("apps.api.middleware.restQueryParser.santizeSkip", () => {
  it("should return 0 if request page is not a positive integer greater than 1", () => {
    expect(santizeSkip({ maxPage, limit, page: 0 })).toBe(0);
    expect(santizeSkip({ maxPage, limit, page: "!@*&!&@(" })).toBe(0);
  });

  it("should return skip number base on max page if request limit is greater than it", () => {
    expect(
      santizeSkip({
        maxPage,
        limit,
        page: 100
      })
    ).toBe((maxPage - 1) * limit);
  });

  it("should return skip number base on  request page for other case", () => {
    const page = 2;
    expect(
      santizeSkip({
        maxPage,
        limit,
        page
      })
    ).toBe((page - 1) * limit);
  });
});
