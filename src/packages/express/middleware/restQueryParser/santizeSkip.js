module.exports = ({ maxPage, page, limit }) => {
  page = Number(page);
  if (!Number.isInteger(page) || page < 1) return 0;

  page = page > maxPage ? maxPage : page;

  return (page - 1) * limit;
};
