const mongoose = require("mongoose");

// Reset to test
process.env.DEFAULT_MONGO_DATABASE = "";
const connectMongo = require("./mongoose");

mongoose.connect = jest.fn();
mongoose.model = jest.fn();

const schema = {
  name: "User",
  schema: new mongoose.Schema({ username: String, password: String })
};

describe("mongoose", () => {
  beforeEach(() => {
    mongoose.connect.mockClear();
    mongoose.model.mockClear();
  });

  it("should throw error because of missing schemas", () => {
    expect(connectMongo).toThrow(
      new Error("You must provide non-empty array of schemas!")
    );
  });

  it("should using default variable to connect to mongo", () => {
    connectMongo({ schemas: [schema] });

    expect(mongoose.connect).toHaveBeenCalledTimes(1);
    expect(mongoose.connect).toHaveBeenCalledWith("mongodb://localhost:27017", {
      dbName: "mongoose",
      useCreateIndex: true,
      useFindAndModify: false,
      useNewUrlParser: true
    });

    expect(mongoose.model).toHaveBeenCalledTimes(1);
    expect(mongoose.model).toHaveBeenCalledWith(schema.name, schema.schema);
  });

  it("should connect to mongo with authentication information", () => {
    const connection = {
      host: "127.0.0.1",
      port: 8001,
      dbName: "api",
      options: {
        user: "devteam",
        pass: "idontknowthatpassword",
        authSource: "admin",
        useCreateIndex: false
      }
    };

    connectMongo({ connection, schemas: [schema] });

    expect(mongoose.connect).toHaveBeenCalledTimes(1);
    expect(mongoose.connect).toHaveBeenCalledWith(
      `mongodb://${connection.host}:${connection.port}`,
      {
        dbName: connection.dbName,
        user: connection.options.user,
        pass: connection.options.pass,
        authSource: connection.options.authSource,
        useCreateIndex: connection.options.useCreateIndex,
        useFindAndModify: false,
        useNewUrlParser: true
      }
    );

    expect(mongoose.model).toHaveBeenCalledTimes(1);
    expect(mongoose.model).toHaveBeenCalledWith(schema.name, schema.schema);
  });
});
