const mongoose = require("mongoose");

const DEFAULT_CONNECTION = {
  host:
    /* istanbul ignore next */ process.env.DEFAULT_MONGO_HOST || "localhost",
  port:
    /* istanbul ignore next */ Number(process.env.DEFAULT_MONGO_PORT) || 27017,
  dbName:
    /* istanbul ignore next */ process.env.DEFAULT_MONGO_DATABASE || "mongoose"
};
const DEFAULT_CONNECTION_OPTIONS = {
  useCreateIndex: true,
  useFindAndModify: false,
  useNewUrlParser: true
};

module.exports = ({ connection, schemas } = {}) => {
  connection = { ...DEFAULT_CONNECTION, ...connection };

  if (!Array.isArray(schemas) || !schemas.length) {
    throw new Error("You must provide non-empty array of schemas!");
  }

  const uri = `mongodb://${connection.host}:${connection.port}`;
  let options = {
    ...DEFAULT_CONNECTION_OPTIONS,
    dbName: connection.dbName,
    ...connection.options
  };
  mongoose.connect(
    uri,
    options
  );

  return schemas.reduce(
    (models, { name, schema }) => ({
      ...models,
      [name]: mongoose.model(name, schema)
    }),
    {}
  );
};
module.exports.DEFAULT_CONNECTION = DEFAULT_CONNECTION;
module.exports.DEFAULT_CONNECTION_OPTIONS = DEFAULT_CONNECTION_OPTIONS;
