const winston = require("winston");

const enumerateErrorFormatter = require("./enumerateErrorFormatter");
const consoleFormatter = require("./consoleFormatter");

const DEFAULT_OPTIONS = {
  debug: !["prod", "production"].includes(process.env.NODE_ENV),
  level: "info",
  defaultMeta: { appName: process.env.npm_package_name }
};

module.exports = opts => {
  const { debug, level, defaultMeta } = { ...DEFAULT_OPTIONS, ...opts };

  const logger = winston.createLogger({
    level,
    defaultMeta,
    format: winston.format.combine(
      enumerateErrorFormatter(defaultMeta)(),
      winston.format.timestamp(),
      winston.format.json()
    ),
    transports: [new winston.transports.File({ filename: "logs/combined.log" })]
  });

  if (debug) {
    logger.add(
      new winston.transports.Console({
        format: winston.format.combine(
          enumerateErrorFormatter(defaultMeta)(),
          winston.format.colorize(),
          winston.format.simple(),
          consoleFormatter()
        )
      })
    );
  }

  return logger;
};
