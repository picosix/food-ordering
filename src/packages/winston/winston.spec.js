const winston = require("winston");

const createLogger = require("./winston");

const fakeLogger = { add: jest.fn() };
jest.mock("winston", () => ({
  transports: {
    File: jest.fn().mockImplementation(() => ({ name: "transports.File" })),
    Console: jest
      .fn()
      .mockImplementation(() => ({ name: "transports.Console" }))
  },
  format: {
    combine: jest.fn((...func) => func),
    timestamp: jest.fn(() => "format.timestamp"),
    json: jest.fn(() => "format.json"),
    colorize: jest.fn(() => "format.colorize"),
    simple: jest.fn(() => "format.simple")
  }
}));
jest.mock("./enumerateErrorFormatter", () =>
  jest.fn(() => () => "format.enumerateErrorFormatter")
);
jest.mock("./consoleFormatter", () => jest.fn(() => "format.consoleFormatter"));

describe("winston", () => {
  beforeAll(() => {
    winston.createLogger = jest.fn().mockImplementation(() => fakeLogger);
  });

  beforeEach(() => {
    fakeLogger.add.mockClear();
    winston.createLogger.mockClear();
    winston.transports.File.mockClear();
    winston.transports.Console.mockClear();
    winston.format.combine.mockClear();
    winston.format.timestamp.mockClear();
    winston.format.json.mockClear();
    winston.format.colorize.mockClear();
    winston.format.simple.mockClear();
  });

  it("should create logger with default configurations", () => {
    createLogger();

    expect(winston.createLogger).toHaveBeenCalledTimes(1);
    expect(winston.createLogger).toHaveBeenCalledWith({
      level: "info",
      defaultMeta: { appName: process.env.npm_package_name },
      format: [
        "format.enumerateErrorFormatter",
        "format.timestamp",
        "format.json"
      ],
      transports: [{ name: "transports.File" }]
    });

    expect(winston.transports.File).toHaveBeenCalledTimes(1);
    expect(winston.transports.File).toHaveBeenCalledWith({
      filename: "logs/combined.log"
    });

    expect(fakeLogger.add).toHaveBeenCalledTimes(1);
    expect(fakeLogger.add).toHaveBeenCalledWith({ name: "transports.Console" });

    expect(winston.transports.Console).toHaveBeenCalledTimes(1);
    expect(winston.transports.Console).toHaveBeenCalledWith({
      format: [
        "format.enumerateErrorFormatter",
        "format.colorize",
        "format.simple",
        "format.consoleFormatter"
      ]
    });
  });

  it("should ONLY create file logger on production", () => {
    createLogger({ debug: false });

    expect(winston.createLogger).toHaveBeenCalledTimes(1);
    expect(winston.createLogger).toHaveBeenCalledWith({
      level: "info",
      defaultMeta: { appName: process.env.npm_package_name },
      format: [
        "format.enumerateErrorFormatter",
        "format.timestamp",
        "format.json"
      ],
      transports: [{ name: "transports.File" }]
    });

    expect(winston.transports.File).toHaveBeenCalledTimes(1);
    expect(winston.transports.File).toHaveBeenCalledWith({
      filename: "logs/combined.log"
    });

    expect(fakeLogger.add).not.toBeCalled();
    expect(winston.transports.Console).not.toBeCalled();
  });
});
