const formatter = require("./formatter");

const defaultMeta = { appName: "logger" };

describe("enumerateErrorFormatter.formatter", () => {
  it("should format info.message if is error object", () => {
    const info = { message: new Error("Error!") };

    expect(formatter(info, defaultMeta)).toEqual({
      ...defaultMeta,
      message: {
        ...defaultMeta,
        message: info.message.message,
        stack: info.message.stack
      }
    });
  });

  it("should format info if is error object", () => {
    const info = new Error("Error!");

    expect(formatter(info, defaultMeta)).toEqual({
      ...defaultMeta,
      message: info.message,
      stack: info.stack
    });
  });

  it("should just merge defaultMeta and info on other case", () => {
    const info = { message: "Notice me!!!" };

    expect(formatter(info, defaultMeta)).toEqual({
      ...defaultMeta,
      ...info
    });
  });
});
