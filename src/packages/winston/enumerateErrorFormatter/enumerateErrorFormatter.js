const winston = require("winston");

const formatter = require("./formatter");
// On winston 3.1.0 defaultMeta is not implemented by default
// So, I should implement it by myself
module.exports = defaultMeta =>
  winston.format(info => formatter(info, defaultMeta));
