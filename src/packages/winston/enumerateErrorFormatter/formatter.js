module.exports = (info, defaultMeta) => {
  if (info.message instanceof Error) {
    info.message = {
      ...defaultMeta,
      message: info.message.message,
      stack: info.message.stack,
      ...info.message
    };
  }

  if (info instanceof Error) {
    return {
      ...defaultMeta,
      message: info.message,
      stack: info.stack,
      ...info
    };
  }

  return { ...defaultMeta, ...info };
};
