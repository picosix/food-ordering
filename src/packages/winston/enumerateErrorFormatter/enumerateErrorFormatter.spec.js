const winston = require("winston");

const enumerateErrorFormatter = require("./enumerateErrorFormatter");
const formatter = require("./formatter");

jest.mock("winston", () => ({
  format: jest.fn()
}));
jest.mock("./formatter");

describe("enumerateErrorFormatter.enumerateErrorFormatter", () => {
  beforeEach(() => {
    formatter.mockClear();
    winston.format.mockClear();
  });

  it("should format erorr object to friendly format for human", () => {
    winston.format.mockImplementationOnce(func => func());

    enumerateErrorFormatter();

    expect(formatter).toHaveBeenCalledTimes(1);

    expect(winston.format).toHaveBeenCalledTimes(1);
    expect(winston.format).toHaveBeenCalledWith(expect.any(Function));
  });
});
