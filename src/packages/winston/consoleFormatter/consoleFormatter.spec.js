const winston = require("winston");

const consoleFormatter = require("./consoleFormatter");
const formatter = require("./formatter");

jest.mock("winston", () => ({
  format: {
    printf: jest.fn()
  }
}));
jest.mock("./formatter");

describe("consoleFormatter.consoleFormatter", () => {
  beforeEach(() => {
    formatter.mockClear();
    winston.format.printf.mockClear();
  });

  it("should format message to friendly format for human", () => {
    winston.format.printf.mockImplementationOnce(func => func());

    consoleFormatter();

    expect(formatter).toHaveBeenCalledTimes(1);

    expect(winston.format.printf).toHaveBeenCalledTimes(1);
    expect(winston.format.printf).toHaveBeenCalledWith(expect.any(Function));
  });
});
