const formatter = require("./formatter");

describe("consoleFormatter.formatter", () => {
  it("should return formatted string", () => {
    const info = {
      timestamp: Math.floor(Date.now() / 1000),
      level: "info",
      message: "Notice me!!!"
    };

    expect(formatter(info)).toBe(
      `${info.timestamp} ${info.level}: ${info.message}`
    );
  });

  it("should return formatted string of error object (include stack)", () => {
    let info = new Error("Error!");

    info = {
      ...info,
      timestamp: Math.floor(Date.now() / 1000),
      level: "info"
    };

    expect(formatter(info)).toBe(
      `${info.timestamp} ${info.level}: ${info.stack}`
    );
  });
});
