module.exports = info =>
  `${info.timestamp} ${info.level}: ${info.stack || info.message}`;
