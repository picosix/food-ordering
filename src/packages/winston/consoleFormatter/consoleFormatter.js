const winston = require("winston");

const formatter = require("./formatter");
module.exports = () => winston.format.printf(formatter);
