const express = require("./packages/express");
const createLogger = require("./packages/winston");

const config = require("../config");

// Database
const generateModels = require("./models");

const opts = {
  rateLimiter: { redis: config.database.redis },
  restQueryParser: config.restQueryParser,
  settings: config,
  logger: createLogger(),
  models: generateModels()
};
const server = express(opts);

module.exports = server;
