module.exports = () => (error, req, res) => {
  let debug = req.app.get("settings").debug;
  let logger = req.app.get("logger");

  logger.error(error);
  let jsonResponse;

  if (debug) {
    jsonResponse = { message: error.message, stack: error.stack };
  } else {
    jsonResponse = {
      message: "Something went wrong! Please try again later ;("
    };
  }

  return res.status(500).json(jsonResponse);
};
