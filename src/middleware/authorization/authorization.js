const Acl = require("acl");
const size = require("lodash/size");
const uniq = require("lodash/uniq");

module.exports = opts => {
  // eslint-disable-next-line new-cap
  const acl = new Acl(new Acl.memoryBackend());

  const { grantAccess, permissionsMapping, publicPaths } = { ...opts };

  const mw = async (req, res, next) => {
    if (!acl || !size(grantAccess) || !size(permissionsMapping)) {
      return next(new Error("ACL is not configured"));
    }

    if (!req.app.get("allowedRoleIds")) {
      const allowedRoleIds = uniq(
        grantAccess.reduce((roleIds, { roles }) => [...roleIds, ...roles], [])
      );
      req.app.set("allowedRoleIds", allowedRoleIds);
    }

    const [permission] = permissionsMapping.filter(
      permission =>
        permission.method === req.method &&
        new RegExp(permission.url).test(req.path)
    );
    if (!permission) return res.status(403).json({ message: "Forbidden" });

    await acl.allow(grantAccess);
    await acl.addUserRoles(
      req._userWithAuthProps.username,
      req._userWithAuthProps.roleId
    );

    const isAllowed = await acl.isAllowed(
      req._userWithAuthProps.username,
      permission.resourcesName,
      permission.actionName
    );

    if (!isAllowed) return res.status(403).json({ message: "Forbidden" });

    return next();
  };

  if (Array.isArray(publicPaths) && publicPaths.length) {
    mw.unless = require("express-unless");
    return mw.unless({ path: publicPaths });
  }
  return mw;
};
