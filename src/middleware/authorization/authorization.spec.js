const Acl = require("acl");
const faker = require("faker");
const unless = require("express-unless");
const uniq = require("lodash/uniq");

const authorization = require("./authorization");

jest.mock("acl");
jest.mock("express-unless");

const publicPaths = [new RegExp("^/auth/local(?!(/me))$", "i")];
const fakeAcl = {
  allow: jest.fn(),
  isAllowed: jest.fn(),
  addUserRoles: jest.fn()
};
const opts = {
  acl: fakeAcl,
  grantAccess: [
    {
      roles: ["ADMINISTRATOR"],
      allows: [
        {
          resources: ["users", "auth", "orders", "dishes"],
          permissions: "*"
        }
      ]
    },
    {
      roles: ["CUSTOMER"],
      allows: [
        { resources: "auth", permissions: ["register", "login"] },
        { resources: "orders", permissions: ["place"] },
        { resources: "dishes", permissions: ["find", "findById"] }
      ]
    },
    {
      roles: ["CHEF"],
      allows: [
        { resources: "orders", permissions: ["find"] },
        { resources: "dishes", permissions: ["findNext"] }
      ]
    }
  ],
  permissionsMapping: [
    {
      resourcesName: "users",
      actionName: "create",
      url: /\/users\/?/,
      method: "POST"
    },
    {
      resourcesName: "users",
      actionName: "find",
      url: /\/users\/?/,
      method: "GET"
    },
    {
      resourcesName: "users",
      actionName: "findByUsername",
      url: /\/users\/[_\d+\w+]+\/?/,
      method: "GET"
    },
    {
      resourcesName: "users",
      actionName: "updateByUsername",
      url: /\/users\/[_\d+\w+]+\/?/,
      method: "PUT"
    },
    {
      resourcesName: "users",
      actionName: "deleteByUsername",
      url: /\/users\/[_\d+\w+]+\/?/,
      method: "DELETE"
    }
  ]
};
const getterMapping = {
  allowedRoleIds: uniq(
    opts.grantAccess.reduce((roleIds, { roles }) => [...roleIds, ...roles], [])
  )
};
const req = {
  app: { get: jest.fn(k => getterMapping[k]), set: jest.fn() }
};
const res = { status: jest.fn(() => res), json: jest.fn(() => res) };
const next = jest.fn();
const userWithAuthProps = {
  username: faker.internet.userName(),
  roleId: "ADMINISTRATOR"
};

describe("src.auth.authorization.acl", () => {
  beforeAll(() => {
    Acl.mockImplementation(() => fakeAcl);
    Acl.memoryBackend = jest.fn(() => ({ name: "Acl.memoryBackend" }));
  });

  beforeEach(() => {
    Acl.mockClear();
    Acl.memoryBackend.mockClear();
    fakeAcl.allow.mockClear();
    fakeAcl.isAllowed.mockClear();
    fakeAcl.addUserRoles.mockClear();
    req.app.get.mockClear();
    req.app.set.mockClear();
    res.status.mockClear();
    res.json.mockClear();
    next.mockClear();
    unless.mockClear();
  });

  it("should authorize successful", async () => {
    fakeAcl.isAllowed.mockResolvedValueOnce(true);

    await authorization(opts)(
      {
        ...req,
        method: "POST",
        path: "/users",
        _userWithAuthProps: userWithAuthProps
      },
      res,
      next
    );

    expect(Acl.memoryBackend).toHaveBeenCalledTimes(1);
    expect(Acl).toHaveBeenCalledTimes(1);
    expect(Acl).toHaveBeenCalledWith({ name: "Acl.memoryBackend" });

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("allowedRoleIds");
    expect(req.app.set).not.toBeCalled();

    expect(fakeAcl.allow).toHaveBeenCalledTimes(1);
    expect(fakeAcl.allow).toHaveBeenCalledWith(opts.grantAccess);

    expect(fakeAcl.addUserRoles).toHaveBeenCalledTimes(1);
    expect(fakeAcl.addUserRoles).toHaveBeenCalledWith(
      userWithAuthProps.username,
      userWithAuthProps.roleId
    );

    expect(fakeAcl.isAllowed).toHaveBeenCalledTimes(1);
    expect(fakeAcl.isAllowed).toHaveBeenCalledWith(
      userWithAuthProps.username,
      "users",
      "create"
    );

    expect(res.status).not.toBeCalled();
    expect(res.json).not.toBeCalled();

    expect(next).toHaveBeenCalledTimes(1);
    expect(unless).not.toBeCalled();
  });

  it("should move to error handler if ACL is not configured", async () => {
    await authorization()(req, res, next);

    expect(Acl).toHaveBeenCalledTimes(1);
    expect(Acl).toHaveBeenCalledWith({ name: "Acl.memoryBackend" });
    expect(Acl.memoryBackend).toHaveBeenCalledTimes(1);

    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith(new Error("ACL is not configured"));

    expect(req.app.get).not.toBeCalled();
    expect(req.app.set).not.toBeCalled();

    expect(res.status).not.toBeCalled();
    expect(res.json).not.toBeCalled();
    expect(fakeAcl.allow).not.toBeCalled();
    expect(fakeAcl.addUserRoles).not.toBeCalled();
    expect(fakeAcl.isAllowed).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should return error because permission of request url is not configured", async () => {
    await authorization(opts)(
      { ...req, method: "DELETE", path: "/users" },
      res,
      next
    );

    expect(Acl).toHaveBeenCalledTimes(1);
    expect(Acl).toHaveBeenCalledWith({ name: "Acl.memoryBackend" });
    expect(Acl.memoryBackend).toHaveBeenCalledTimes(1);

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("allowedRoleIds");
    expect(req.app.set).not.toBeCalled();

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(403);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: "Forbidden" });

    expect(fakeAcl.allow).not.toBeCalled();
    expect(fakeAcl.addUserRoles).not.toBeCalled();
    expect(fakeAcl.isAllowed).not.toBeCalled();
    expect(next).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should return error user is not granted access to resource", async () => {
    const userWithAuthProps = { username: faker.internet.userName() };
    fakeAcl.isAllowed.mockResolvedValueOnce(false);

    await authorization(opts)(
      {
        ...req,
        method: "POST",
        path: "/users",
        _userWithAuthProps: userWithAuthProps
      },
      res,
      next
    );

    expect(Acl).toHaveBeenCalledTimes(1);
    expect(Acl).toHaveBeenCalledWith({ name: "Acl.memoryBackend" });
    expect(Acl.memoryBackend).toHaveBeenCalledTimes(1);

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("allowedRoleIds");
    expect(req.app.set).not.toBeCalled();

    expect(fakeAcl.allow).toHaveBeenCalledTimes(1);
    expect(fakeAcl.allow).toHaveBeenCalledWith(opts.grantAccess);

    expect(fakeAcl.addUserRoles).toHaveBeenCalledTimes(1);
    expect(fakeAcl.addUserRoles).toHaveBeenCalledWith(
      userWithAuthProps.username,
      userWithAuthProps.roleId
    );

    expect(fakeAcl.isAllowed).toHaveBeenCalledTimes(1);
    expect(fakeAcl.isAllowed).toHaveBeenCalledWith(
      userWithAuthProps.username,
      "users",
      "create"
    );

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(403);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: "Forbidden" });

    expect(next).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should  set allowedRoleIds if it was not set", async () => {
    const userWithAuthProps = { username: faker.internet.userName() };
    req.app.get.mockReturnValueOnce(null);
    fakeAcl.isAllowed.mockResolvedValueOnce(false);

    await authorization(opts)(
      {
        ...req,
        method: "POST",
        path: "/users",
        _userWithAuthProps: userWithAuthProps
      },
      res,
      next
    );

    expect(Acl).toHaveBeenCalledTimes(1);
    expect(Acl).toHaveBeenCalledWith({ name: "Acl.memoryBackend" });
    expect(Acl.memoryBackend).toHaveBeenCalledTimes(1);

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("allowedRoleIds");
    expect(req.app.set).toHaveBeenCalledTimes(1);
    expect(req.app.set).toHaveBeenCalledWith(
      "allowedRoleIds",
      getterMapping.allowedRoleIds
    );

    expect(fakeAcl.allow).toHaveBeenCalledTimes(1);
    expect(fakeAcl.allow).toHaveBeenCalledWith(opts.grantAccess);

    expect(fakeAcl.addUserRoles).toHaveBeenCalledTimes(1);
    expect(fakeAcl.addUserRoles).toHaveBeenCalledWith(
      userWithAuthProps.username,
      userWithAuthProps.roleId
    );

    expect(fakeAcl.isAllowed).toHaveBeenCalledTimes(1);
    expect(fakeAcl.isAllowed).toHaveBeenCalledWith(
      userWithAuthProps.username,
      "users",
      "create"
    );

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(403);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: "Forbidden" });

    expect(next).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should excludes publicPaths if it was set", async () => {
    unless.mockReturnValueOnce({ name: "unless" });

    expect(authorization({ publicPaths, ...opts })).toEqual({ name: "unless" });

    expect(unless).toHaveBeenCalledTimes(1);
    expect(unless).toHaveBeenCalledWith({ path: publicPaths });
  });
});
