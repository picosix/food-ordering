const errorHandlerMiddleware = require("./errorHandler");

const error = new Error("Unknow");
const fakeLogger = { info: jest.fn(), warn: jest.fn(), error: jest.fn() };
const get = {
  settings: { debug: true },
  logger: fakeLogger
};
const fakeApp = {
  get: jest.fn(key => get[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("apps.api.middleware.errorHandler", () => {
  beforeEach(() => {
    fakeLogger.info.mockClear();
    fakeLogger.warn.mockClear();
    fakeLogger.error.mockClear();

    fakeApp.get.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return full error information if this is debug mode", async () => {
    const req = {
      app: fakeApp
    };

    errorHandlerMiddleware()(error, req, res);

    expect(fakeApp.get).toHaveBeenCalledTimes(2);
    expect(fakeApp.get.mock.calls[0][0]).toBe("settings");
    expect(fakeApp.get.mock.calls[1][0]).toBe("logger");

    expect(fakeLogger.error).toHaveBeenCalledTimes(1);
    expect(fakeLogger.error).toHaveBeenCalledWith(error);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(500);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: error.message,
      stack: error.stack
    });
  });

  it("should return friendly response for other case", async () => {
    fakeApp.get
      .mockReturnValueOnce({ debug: false })
      .mockReturnValueOnce(fakeLogger);
    const req = {
      app: fakeApp
    };

    errorHandlerMiddleware()(error, req, res);

    expect(fakeApp.get).toHaveBeenCalledTimes(2);
    expect(fakeApp.get.mock.calls[0][0]).toBe("settings");
    expect(fakeApp.get.mock.calls[1][0]).toBe("logger");

    expect(fakeLogger.error).toHaveBeenCalledTimes(1);
    expect(fakeLogger.error).toHaveBeenCalledWith(error);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(500);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: "Something went wrong! Please try again later ;("
    });
  });
});
