const faker = require("faker");
const unless = require("express-unless");

const fromCache = require("./fromCache");

jest.mock("express-unless");

const opts = {
  publicPaths: [new RegExp("^/auth/local(?!(/me))$", "i")],
  cacher: { get: jest.fn() }
};
const instances = {
  logger: { error: jest.fn() }
};
const app = { get: jest.fn(k => instances[k]) };
const fakeUser = { useranme: faker.internet.userName() };
const user = { payload: { useranme: fakeUser.useranme } };
const req = { app };
const res = {};
const next = jest.fn();

describe("src.auth.authentication.resolveUserAuthProps.fromCache", () => {
  beforeEach(() => {
    opts.cacher.get.mockClear();
    instances.logger.error.mockClear();
    app.get.mockClear();
    next.mockClear();
    unless.mockClear();
  });

  it("should move to next route if req.user is not set", async () => {
    await fromCache({ cacher: opts.cacher })(req, res, next);

    expect(next).toHaveBeenCalledTimes(1);
    expect(opts.cacher.get).not.toBeCalled();
    expect(instances.logger.error).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should set req._userWithAuthProps by value from cache", async () => {
    opts.cacher.get.mockResolvedValueOnce(JSON.stringify(fakeUser));

    await fromCache({ cacher: opts.cacher })({ ...req, user }, res, next);

    expect(opts.cacher.get).toHaveBeenCalledTimes(1);
    expect(opts.cacher.get).toHaveBeenCalledWith(user.payload.useranme);

    expect(next).toHaveBeenCalledTimes(1);

    expect(req.app.get).not.toBeCalled();
    expect(instances.logger.error).not.toBeCalled();

    expect(unless).not.toBeCalled();
  });

  it("should log error and move to next route when parsed is error", async () => {
    opts.cacher.get.mockResolvedValueOnce("");

    await fromCache({ cacher: opts.cacher })({ ...req, user }, res, next);

    expect(opts.cacher.get).toHaveBeenCalledTimes(1);
    expect(opts.cacher.get).toHaveBeenCalledWith(user.payload.useranme);

    expect(instances.logger.error).toHaveBeenCalledTimes(1);
    expect(instances.logger.error).toHaveBeenCalledWith(
      expect.stringContaining("Cache error")
    );

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("logger");

    expect(next).toHaveBeenCalledTimes(1);

    expect(unless).not.toBeCalled();
  });

  it("should excludes publicPaths if it was set", async () => {
    opts.cacher.get.mockResolvedValueOnce("");
    unless.mockReturnValueOnce({ name: "unless" });

    expect(fromCache(opts)).toEqual({ name: "unless" });

    expect(unless).toHaveBeenCalledTimes(1);
    expect(unless).toHaveBeenCalledWith({ path: opts.publicPaths });
  });
});
