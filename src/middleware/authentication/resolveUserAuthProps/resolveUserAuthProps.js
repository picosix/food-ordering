const Redis = require("ioredis");
const compose = require("compose-middleware").compose;

const DEFAULT_OPTIONS = {
  prefix: "users:",
  seconds: 2592000 /** 30 * 24 * 3600 -> 30 days */
};

const fromCache = require("./fromCache");
const fromDb = require("./fromDb");

module.exports = opts => {
  if (!opts || !opts.redis) throw new Error("Redis config can not be blank");
  const cacheOpts = { ...DEFAULT_OPTIONS, ...opts.cacheOpts };
  const cacher = new Redis({ ...opts.redis, keyPrefix: cacheOpts.prefix });

  return compose([
    fromCache({ ...opts, cacheOpts, cacher }),
    fromDb({ ...opts, cacheOpts, cacher })
  ]);
};
module.exports.DEFAULT_OPTIONS = DEFAULT_OPTIONS;
