const isObject = require("lodash/isObject");
const isEmpty = require("lodash/isEmpty");

module.exports = ({ cacher, cacheOpts, publicPaths }) => {
  const mw = async (req, res, next) => {
    if (!req.user) return next();

    if (isObject(req._userWithAuthProps) && !isEmpty(req._userWithAuthProps)) {
      return next();
    }

    const { User } = req.app.get("models");
    const { username } = req.user.payload;
    const user = await User.getAuthProps({ username });
    if (!user) {
      return next(new Error("Your account has been disabled"));
    }

    req._userWithAuthProps = user;
    cacher.set(username, JSON.stringify(user), "ex", cacheOpts.seconds);
    return next();
  };

  if (Array.isArray(publicPaths) && publicPaths.length) {
    mw.unless = require("express-unless");
    return mw.unless({ path: publicPaths });
  }
  return mw;
};
