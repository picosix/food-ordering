const faker = require("faker");
const unless = require("express-unless");

const fromDb = require("./fromDb");

jest.mock("express-unless");

const cacheOpts = { seconds: 2592000 };
const opts = {
  publicPaths: [new RegExp("^/auth/local(?!(/me))$", "i")],
  cacher: { set: jest.fn() }
};
const instances = {
  models: { User: { getAuthProps: jest.fn() } }
};
const app = { get: jest.fn(k => instances[k]) };
const fakeUser = { username: faker.internet.userName() };
const user = { payload: { username: fakeUser.username } };
const req = { app };
const res = {};
const next = jest.fn();

describe("src.auth.authentication.resolveUserAuthProps.fromDb", () => {
  beforeEach(() => {
    opts.cacher.set.mockClear();
    instances.models.User.getAuthProps.mockClear();
    app.get.mockClear();
    next.mockClear();
    unless.mockClear();
  });

  it("should move to next route when req.user is not set", async () => {
    await fromDb({ cacher: opts.cacher, cacheOpts })(
      { ...req, _userWithAuthProps: fakeUser },
      res,
      next
    );

    expect(next).toHaveBeenCalledTimes(1);

    expect(req.app.get).not.toBeCalled();
    expect(instances.models.User.getAuthProps).not.toBeCalled();
    expect(opts.cacher.set).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should move to next route when req._userWithAuthProps is set", async () => {
    await fromDb({ cacher: opts.cacher, cacheOpts })(
      { ...req, _userWithAuthProps: fakeUser, user },
      res,
      next
    );

    expect(next).toHaveBeenCalledTimes(1);

    expect(req.app.get).not.toBeCalled();
    expect(instances.models.User.getAuthProps).not.toBeCalled();
    expect(opts.cacher.set).not.toBeCalled();
    expect(unless).not.toBeCalled();
  });

  it("should set req._userWithAuthProps and set cache from user on db", async () => {
    instances.models.User.getAuthProps.mockResolvedValueOnce(fakeUser);

    await fromDb({ cacher: opts.cacher, cacheOpts })(
      { ...req, user },
      res,
      next
    );

    expect(next).toHaveBeenCalledTimes(1);

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("models");

    expect(instances.models.User.getAuthProps).toHaveBeenCalledTimes(1);
    expect(instances.models.User.getAuthProps).toHaveBeenCalledWith({
      username: user.payload.username
    });

    expect(opts.cacher.set).toHaveBeenCalledTimes(1);
    expect(opts.cacher.set).toHaveBeenCalledWith(
      user.payload.username,
      JSON.stringify(fakeUser),
      "ex",
      cacheOpts.seconds
    );

    expect(next).toHaveBeenCalledTimes(1);
    expect(unless).not.toBeCalled();
  });

  it("should move to error handler if user was not found", async () => {
    instances.models.User.getAuthProps.mockResolvedValueOnce(null);

    await fromDb({ cacher: opts.cacher, cacheOpts })(
      { ...req, user },
      res,
      next
    );

    expect(next).toHaveBeenCalledTimes(1);

    expect(req.app.get).toHaveBeenCalledTimes(1);
    expect(req.app.get).toHaveBeenCalledWith("models");

    expect(instances.models.User.getAuthProps).toHaveBeenCalledTimes(1);
    expect(instances.models.User.getAuthProps).toHaveBeenCalledWith({
      username: user.payload.username
    });

    expect(opts.cacher.set).not.toBeCalled();

    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith(
      new Error("Your account has been disabled")
    );

    expect(unless).not.toBeCalled();
  });

  it("should excludes publicPaths if it was set", async () => {
    unless.mockReturnValueOnce({ name: "unless" });

    expect(fromDb(opts)).toEqual({ name: "unless" });

    expect(unless).toHaveBeenCalledTimes(1);
    expect(unless).toHaveBeenCalledWith({ path: opts.publicPaths });
  });
});
