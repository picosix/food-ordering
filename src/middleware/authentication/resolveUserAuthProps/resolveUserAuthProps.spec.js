const Redis = require("ioredis");
const compose = require("compose-middleware").compose;

const resolveUserAuthProps = require("./resolveUserAuthProps");
const fromCache = require("./fromCache");
const fromDb = require("./fromDb");

jest.mock("ioredis");
jest.mock("compose-middleware");
jest.mock("./fromCache");
jest.mock("./fromDb");

const opts = {
  redis: { host: "localhost", port: 6379 },
  cacheOpts: { prefix: "users", seconds: 2592000 }
};
const fakeRedis = { name: "ioredis" };

describe("src.auth.authentication.resolveUserAuthProps.resolveUserAuthProps", () => {
  beforeAll(() => {
    Redis.mockImplementation(() => fakeRedis);
  });
  beforeEach(() => {
    Redis.mockClear();
    compose.mockClear();
    fromCache.mockClear();
    fromDb.mockClear();
  });

  it("should throw error because of missing redis config", () => {
    expect(resolveUserAuthProps).toThrow(
      new Error("Redis config can not be blank")
    );

    expect(Redis).not.toBeCalled();
    expect(compose).not.toBeCalled();
    expect(fromCache).not.toBeCalled();
    expect(fromDb).not.toBeCalled();
  });

  it("should return composed middleware from 2 middlewares fromCache and fromDb", () => {
    resolveUserAuthProps(opts);

    expect(Redis).toHaveBeenCalledTimes(1);
    expect(Redis).toHaveBeenCalledWith({
      ...opts.redis,
      keyPrefix: opts.cacheOpts.prefix
    });

    expect(fromCache).toHaveBeenCalledTimes(1);
    expect(fromCache).toHaveBeenCalledWith({ cacher: fakeRedis, ...opts });

    expect(fromDb).toHaveBeenCalledTimes(1);
    expect(fromDb).toHaveBeenCalledWith({
      cacher: fakeRedis,
      ...opts
    });
  });
});
