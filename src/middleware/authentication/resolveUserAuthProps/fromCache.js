module.exports = ({ cacher, publicPaths }) => {
  const mw = async (req, res, next) => {
    if (!req.user) return next();

    try {
      req._userWithAuthProps = JSON.parse(
        await cacher.get(req.user.payload.useranme)
      );
      return next();
    } catch (error) {
      req.app.get("logger").error(`Cache error: ${error.stack}`);
      return next();
    }
  };

  if (Array.isArray(publicPaths) && publicPaths.length) {
    mw.unless = require("express-unless");
    return mw.unless({ path: publicPaths });
  }

  return mw;
};
