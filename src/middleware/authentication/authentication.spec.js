const compose = require("compose-middleware").compose;

const authentication = require("./authentication");
const local = require("./local");
const resolveUserAuthProps = require("./resolveUserAuthProps");

jest.mock("compose-middleware");
jest.mock("./local", () => jest.fn(() => ({ name: "local" })));
jest.mock("./resolveUserAuthProps", () =>
  jest.fn(() => ({ name: "resolveUserAuthProps" }))
);

describe("src.auth.authentication.authentication", () => {
  it("should return composed 2 middlewares: local and resolveUserAuthProps", () => {
    const opts = {};
    authentication(opts);

    expect(local).toHaveBeenCalledTimes(1);
    expect(local).toHaveBeenCalledWith(opts);

    expect(resolveUserAuthProps).toHaveBeenCalledTimes(1);
    expect(resolveUserAuthProps).toHaveBeenCalledWith(opts);

    expect(compose).toHaveBeenCalledTimes(1);
    expect(compose).toHaveBeenCalledWith([
      { name: "local" },
      { name: "resolveUserAuthProps" }
    ]);
  });
});
