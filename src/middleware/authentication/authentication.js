const compose = require("compose-middleware").compose;

const local = require("./local");
const resolveUserAuthProps = require("./resolveUserAuthProps");

module.exports = opts => {
  return compose([local(opts), resolveUserAuthProps(opts)]);
};
