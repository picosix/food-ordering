const jwt = require("express-jwt");

const localAuth = require("./local");
const getTokenfromHeaderOrQuerystring = require("./getTokenfromHeaderOrQuerystring");

jest.mock("express-jwt");
jest.mock("./getTokenfromHeaderOrQuerystring");

const fakeJWT = { unless: jest.fn() };
jwt.mockImplementation(() => fakeJWT);

describe("src.middleware.authentication.local.local", () => {
  describe("local", () => {
    const secretKey = "123";
    const publicPaths = ["/auth"];

    beforeEach(() => {
      jwt.mockClear();
      fakeJWT.unless.mockClear();
    });

    it("should return jwt middleware with DEFAULT options", () => {
      localAuth();

      expect(jwt).toHaveBeenCalledTimes(1);
      expect(jwt).toHaveBeenCalledWith({
        ...localAuth.DEFAULT_OPTIONS,
        secret: undefined,
        getToken: getTokenfromHeaderOrQuerystring
      });

      expect(fakeJWT.unless).not.toBeCalled();
    });

    it("should return jwt middleware with options", () => {
      localAuth({ secretKey, publicPaths });

      expect(jwt).toHaveBeenCalledTimes(1);
      expect(jwt).toHaveBeenCalledWith({
        ...localAuth.DEFAULT_OPTIONS,
        secret: secretKey,
        getToken: getTokenfromHeaderOrQuerystring
      });

      expect(fakeJWT.unless).toHaveBeenCalledTimes(1);
      expect(fakeJWT.unless).toHaveBeenCalledWith({
        path: publicPaths
      });
    });
  });
});
