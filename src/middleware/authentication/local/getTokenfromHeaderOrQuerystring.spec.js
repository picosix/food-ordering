const getTokenfromHeaderOrQuerystring = require("./getTokenfromHeaderOrQuerystring");

describe("src.middleware.authentication.local.getTokenfromHeaderOrQuerystring", () => {
  let accessToken = "abc123xyz";

  it("should return accessToken from header", () => {
    let req = {
      headers: {
        authorization: `Bearer ${accessToken}`
      }
    };

    expect(getTokenfromHeaderOrQuerystring(req)).toBe(accessToken);
  });

  it("should return accessToken from query", () => {
    let req = {
      query: {
        accessToken
      }
    };

    expect(getTokenfromHeaderOrQuerystring(req)).toBe(accessToken);
  });

  it("should return NULl for other case", () => {
    let req = {
      headers: {
        authorization: accessToken
      }
    };

    expect(getTokenfromHeaderOrQuerystring(req)).toBeNull();
  });
});
