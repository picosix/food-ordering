module.exports = req => {
  const token =
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization.split(" ");
  if (token && token[0] === "Bearer") {
    return token[1];
  }

  if (req.query && req.query.accessToken) {
    return req.query.accessToken;
  }

  return null;
};
