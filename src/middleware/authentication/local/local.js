const jwt = require("express-jwt");

const getTokenfromHeaderOrQuerystring = require("./getTokenfromHeaderOrQuerystring");

module.exports = opts => {
  const { publicPaths, secretKey } = { ...opts };

  const jwtMiddleware = jwt({
    secret: secretKey,
    getToken: getTokenfromHeaderOrQuerystring
  });

  if (Array.isArray(publicPaths) && publicPaths.length) {
    return jwtMiddleware.unless({ path: publicPaths });
  }

  return jwtMiddleware;
};
