module.exports = require("./authentication");
module.exports.local = require("./local");
module.exports.resolveUserAuthProps = require("./resolveUserAuthProps");
