const express = require("express");

const ordersController = require("../controllers/orders");

const router = express.Router();

router.param("id", ordersController.validId);
router.post("/temp", ordersController.createTemp);
router.get("/", ordersController.find);
router.get("/:id", ordersController.getDetails);
router.patch("/:id", ordersController.place);
router.delete("/:id", ordersController.deleteById);
router.post("/:id/dishes", ordersController.addDishes);
router.get("/:id/dishes", ordersController.findNonCompletedCooking);
router.patch("/:id/dishes/:dishId", ordersController.markDishAsDone);

module.exports = router;
