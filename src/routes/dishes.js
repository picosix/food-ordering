const express = require("express");

const dishesController = require("../controllers/dishes");

const router = express.Router();

router.param("id", dishesController.validId);
router.get("/", dishesController.find);
router.post("/", dishesController.create);
router.get("/:id", dishesController.findById);
router.patch("/:id", dishesController.updateById);
router.delete("/:id", dishesController.deleteById);

module.exports = router;
