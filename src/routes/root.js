const express = require("express");

const router = express.Router();

router.use("/", (req, res) => {
  return res.json(req.app.get("info"));
});

module.exports = router;
