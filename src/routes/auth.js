const express = require("express");

const localController = require("../controllers/auth/local");

const router = express.Router();

router.post("/local", localController.login);
router.post("/local/register", localController.register);
router.get("/local/me", localController.me);

module.exports = router;
