module.exports = (req, res) => {
  return res.status(404).json({
    message: "The requested URL was not found on this server"
  });
};
