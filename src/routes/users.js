const express = require("express");

const usersController = require("../controllers/users");

const router = express.Router();

router.param("username", usersController.validUsername);
router.get("/", usersController.find);
router.post("/", usersController.create);
router.get("/:username", usersController.findByUsername);
router.patch("/:username", usersController.updateByUsername);
router.delete("/:username", usersController.deleteByUsername);

module.exports = router;
