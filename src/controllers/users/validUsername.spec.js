const faker = require("faker");

const users = require("./index");

const fakeUser = {
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR",
  createdAt: new Date()
};

const fakeModel = {
  findOne: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { User: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };
const next = jest.fn();

describe("src.controllers.users.validUsername", () => {
  beforeEach(() => {
    fakeModel.findOne.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    res.json.mockClear();
    res.status.mockClear();
    next.mockClear();
  });

  it("should set req._user by found user", async () => {
    fakeModel.exec.mockReturnValueOnce(fakeUser);

    let req = { app: fakeApp };
    const username = fakeUser.username;
    await users.validUsername(req, res, next, username);

    expect(fakeModel.findOne).toHaveBeenCalledTimes(1);
    expect(fakeModel.findOne).toHaveBeenCalledWith({ username });

    expect(req._user).toEqual(fakeUser);
    expect(next).toHaveBeenCalledTimes(1);
  });

  it("should retrn error because of non exists user", async () => {
    fakeModel.exec.mockReturnValueOnce(null);

    let req = { app: fakeApp };
    const username = fakeUser.username;
    await users.validUsername(req, res, next, username);

    expect(fakeModel.findOne).toHaveBeenCalledTimes(1);
    expect(fakeModel.findOne).toHaveBeenCalledWith({ username });

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(404);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: `Username [${username}] was not exist`
    });
  });
});
