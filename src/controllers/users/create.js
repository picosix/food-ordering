const bcrypt = require("bcrypt");
const pick = require("lodash/pick");

module.exports = async (req, res) => {
  const { User } = req.app.get("models");
  let userProps = pick(req.body, [
    "name",
    "username",
    "password",
    "confirmPassword",
    "roleId"
  ]);
  let existingUser = await User.findByUsername(userProps.username);
  if (existingUser) {
    return res.status(400).json({
      message: "Your username has been taken"
    });
  }

  if (!userProps.password || userProps.password !== userProps.confirmPassword) {
    return res.status(400).json({
      message:
        "Password could not be blank and must be matched with confirmed password"
    });
  }
  delete userProps.confirmPassword;
  const { saltRounds } = req.app.get("settings").auth;
  userProps.password = await bcrypt.hash(userProps.password, saltRounds);

  const allowedRoleIds = req.app.get("allowedRoleIds");
  if (userProps.roleId && !allowedRoleIds.includes(userProps.roleId)) {
    return res.status(400).json({
      message: `Role [${userProps.roleId}] is not supported`
    });
  }

  let createdUser = await User.create(userProps);

  res.json({ data: { username: createdUser.username } });
};
