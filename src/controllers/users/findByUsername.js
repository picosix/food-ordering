const pick = require("lodash/pick");
module.exports = async (req, res) => {
  const { _user: user } = req;

  res.json({
    data: pick(user, ["username", "name", "roleId"])
  });
};
