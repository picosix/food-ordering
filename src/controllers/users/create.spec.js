const faker = require("faker");
const bcrypt = require("bcrypt");

const users = require("./index");

jest.mock("bcrypt");

const rawPassword = "123456";
const fakeUser = {
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR",
  createdAt: new Date()
};

const fakeModel = {
  findByUsername: jest.fn(),
  create: jest.fn(() => fakeModel)
};
const fakeModels = { User: fakeModel };
const settings = { auth: { saltRounds: 10 } };
const mapping = {
  settings,
  models: fakeModels,
  allowedRoleIds: ["ADMINISTRATOR", "CUSTOMER", "CHEF"]
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.users.create", () => {
  beforeEach(() => {
    bcrypt.hash.mockClear();
    fakeModel.findByUsername.mockClear();
    fakeModel.create.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return created user", async () => {
    fakeModel.findByUsername.mockResolvedValueOnce(null);
    bcrypt.hash.mockReturnValueOnce(fakeUser.password);
    fakeModel.create.mockReturnValueOnce(fakeUser);

    let req = {
      body: {
        username: fakeUser.username,
        password: rawPassword,
        confirmPassword: rawPassword,
        roleId: fakeUser.roleId
      },
      app: fakeApp
    };
    await users.create(req, res);

    expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

    expect(bcrypt.hash).toHaveBeenCalledTimes(1);
    expect(bcrypt.hash).toHaveBeenCalledWith(
      rawPassword,
      settings.auth.saltRounds
    );

    expect(fakeModel.create).toHaveBeenCalledTimes(1);
    expect(fakeModel.create).toHaveBeenCalledWith({
      username: fakeUser.username,
      password: fakeUser.password,
      roleId: fakeUser.roleId
    });

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        username: fakeUser.username
      }
    });
  });

  it("should return error because user was exist", async () => {
    fakeModel.findByUsername.mockResolvedValueOnce(fakeUser);

    let req = {
      body: {
        username: fakeUser.username,
        password: rawPassword,
        confirmPassword: rawPassword,
        roleId: fakeUser.roleId
      },
      app: fakeApp
    };
    await users.create(req, res);

    expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

    expect(bcrypt.hash).not.toBeCalled();
    expect(fakeModel.create).not.toBeCalled();

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(400);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: "Your username has been taken"
    });
  });

  it("should return error because password and confirmPassword is invalid", async () => {
    let req = {
      body: {
        username: fakeUser.username,
        password: rawPassword,
        roleId: fakeUser.roleId
      },
      app: fakeApp
    };
    await users.create(req, res);

    expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(400);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message:
        "Password could not be blank and must be matched with confirmed password"
    });
  });

  it("should return error of invalid roleId", async () => {
    let req = {
      body: {
        username: fakeUser.username,
        password: rawPassword,
        confirmPassword: rawPassword,
        roleId: "FAKE_ADMINISTRATOR"
      },
      app: fakeApp
    };
    await users.create(req, res);

    expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(400);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: `Role [${req.body.roleId}] is not supported`
    });
  });
});
