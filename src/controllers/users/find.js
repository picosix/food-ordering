const toLower = require("lodash/toLower");

module.exports = async function find(req, res) {
  const { _paging: paging, query = {} } = req;
  const { username, roleId } = query;
  const { User } = req.app.get("models");
  let queryFind = {};

  if (username) {
    queryFind.username = new RegExp(toLower(username));
  }

  if (roleId) {
    queryFind.roleId = roleId;
  }

  let total = await User.countDocuments(queryFind);
  const sort = { username: 1, ...paging.sort };
  let data = await User.find(queryFind)
    .select({
      _id: 0,
      username: 1,
      name: 1,
      roleId: 1
    })
    .sort(sort)
    .limit(paging.limit)
    .skip(paging.skip)
    .exec();

  return res.json({ total, data });
};
