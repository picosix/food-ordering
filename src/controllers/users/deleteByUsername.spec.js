const faker = require("faker");

const users = require("./index");

const fakeUser = {
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR",
  createdAt: new Date()
};
const fakeModel = {
  deleteOne: jest.fn()
};
const fakeModels = { User: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.user.deleteByUsername", () => {
  beforeEach(() => {
    fakeModel.deleteOne.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return username when deleted user", async () => {
    let req = {
      _user: fakeUser,
      app: fakeApp
    };

    await users.deleteByUsername(req, res);

    expect(fakeModel.deleteOne).toHaveBeenCalledTimes(1);
    expect(fakeModel.deleteOne).toHaveBeenCalledWith({
      username: req._user.username
    });

    expect(res.status).not.toBeCalled();
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        username: req._user.username
      }
    });
  });
});
