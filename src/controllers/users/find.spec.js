const range = require("lodash/range");
const sample = require("lodash/sample");
const faker = require("faker");

const users = require("./index");

const allowedRoleIds = ["ADMINISTRATOR", "CUSTOMER", "CHEF"];
const fakeTotalUsers = range(1, 201);
const fakeUsers = new Array(fakeTotalUsers).fill({}).map(() => ({
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: sample(allowedRoleIds),
  createdAt: new Date()
}));

const fakeModel = {
  find: jest.fn(() => fakeModel),
  sort: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  populate: jest.fn(() => fakeModel),
  limit: jest.fn(() => fakeModel),
  skip: jest.fn(() => fakeModel),
  countDocuments: jest.fn(),
  exec: jest.fn()
};
const fakeModels = { User: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.users.find", () => {
  beforeEach(() => {
    fakeModel.find.mockClear();
    fakeModel.sort.mockClear();
    fakeModel.select.mockClear();
    fakeModel.populate.mockClear();
    fakeModel.limit.mockClear();
    fakeModel.skip.mockClear();
    fakeModel.countDocuments.mockClear();
    fakeModel.exec.mockClear();
    fakeApp.get.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return users on basic query (DEFAULT paging, no filter)", async () => {
    let req = {
      _paging: { limit: 20, skip: 0, sort: {} },
      app: fakeApp
    };
    fakeModel.countDocuments.mockReturnValueOnce(fakeTotalUsers);
    fakeModel.exec.mockReturnValueOnce(fakeUsers.slice(0, req._paging.limit));

    await users.find(req, res);

    expect(fakeModel.countDocuments).toHaveBeenCalledTimes(1);
    expect(fakeModel.countDocuments).toHaveBeenCalledWith({});

    expect(fakeModel.find).toHaveBeenCalledTimes(1);
    expect(fakeModel.find).toHaveBeenCalledWith({});

    expect(fakeModel.select).toHaveBeenCalledTimes(1);
    expect(fakeModel.select).toHaveBeenCalledWith({
      _id: 0,
      username: 1,
      name: 1,
      roleId: 1
    });

    expect(fakeModel.sort).toHaveBeenCalledTimes(1);
    expect(fakeModel.sort).toHaveBeenCalledWith({
      username: 1,
      ...req._paging.sort
    });

    expect(fakeModel.limit).toHaveBeenCalledTimes(1);
    expect(fakeModel.limit).toHaveBeenCalledWith(req._paging.limit);

    expect(fakeModel.skip).toHaveBeenCalledTimes(1);
    expect(fakeModel.skip).toHaveBeenCalledWith(req._paging.skip);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      total: fakeTotalUsers,
      data: fakeUsers.slice(0, req._paging.limit)
    });
  });

  it("should return users with filters (username and roleId)", async () => {
    const [fakeUser] = fakeUsers;
    fakeModel.countDocuments.mockReturnValueOnce(1);
    fakeModel.exec.mockReturnValueOnce([fakeUser]);

    let req = {
      _paging: { limit: 20, skip: 0, sort: { createdAt: -1 } },
      query: {
        username: fakeUser.username,
        roleId: fakeUser.roleId
      },
      app: fakeApp
    };

    await users.find(req, res);

    const query = {
      username: new RegExp(fakeUser.username.toLowerCase()),
      roleId: fakeUser.roleId
    };
    expect(fakeModel.countDocuments).toHaveBeenCalledTimes(1);
    expect(fakeModel.countDocuments).toHaveBeenCalledWith(query);

    expect(fakeModel.find).toHaveBeenCalledTimes(1);
    expect(fakeModel.find).toHaveBeenCalledWith(query);

    expect(fakeModel.select).toHaveBeenCalledTimes(1);
    expect(fakeModel.select).toHaveBeenCalledWith({
      _id: 0,
      username: 1,
      name: 1,
      roleId: 1
    });

    expect(fakeModel.sort).toHaveBeenCalledTimes(1);
    expect(fakeModel.sort).toHaveBeenCalledWith({
      username: 1,
      ...req._paging.sort
    });

    expect(fakeModel.limit).toHaveBeenCalledTimes(1);
    expect(fakeModel.limit).toHaveBeenCalledWith(req._paging.limit);

    expect(fakeModel.skip).toHaveBeenCalledTimes(1);
    expect(fakeModel.skip).toHaveBeenCalledWith(req._paging.skip);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      total: 1,
      data: [fakeUser]
    });
  });
});
