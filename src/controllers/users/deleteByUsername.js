module.exports = async (req, res) => {
  const { User } = req.app.get("models");
  const { _user: user } = req;

  await User.deleteOne({ username: user.username });

  res.json({ data: { username: user.username } });
};
