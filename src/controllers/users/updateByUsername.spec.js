const faker = require("faker");

const users = require("./index");

const fakeUser = {
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR",
  createdAt: new Date(),
  updatedAt: new Date()
};

const fakeModel = {
  findOne: jest.fn(() => fakeModel),
  findOneAndUpdate: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { User: fakeModel };
const mapping = {
  models: fakeModels,
  allowedRoleIds: ["ADMINISTRATOR", "CUSTOMER", "CHEF"]
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.users.updateByUsername", () => {
  beforeEach(() => {
    fakeModel.findOne.mockClear();
    fakeModel.findOneAndUpdate.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should update user successful (role)", async () => {
    fakeModel.exec.mockResolvedValueOnce({
      username: fakeUser.username
    });

    let req = {
      body: {
        roleId: fakeUser.roleId
      },
      _user: {
        ...fakeUser,
        username: faker.internet.userName().toLowerCase(),
        roleId: "CUSTOMER"
      },
      app: fakeApp
    };
    await users.updateByUsername(req, res);

    expect(fakeModel.findOneAndUpdate).toHaveBeenCalledTimes(1);
    expect(fakeModel.findOneAndUpdate).toHaveBeenCalledWith(
      { username: req._user.username },
      {
        $set: {
          email: fakeUser.email,
          roleId: fakeUser.roleId
        }
      },
      { new: true }
    );
    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        username: fakeUser.username,
        email: fakeUser.email
      }
    });
  });

  it("should return error because of invalid value of role", async () => {
    fakeModel.exec.mockResolvedValueOnce(null);

    let req = {
      body: {
        roleId: "FAKE_ADMINISTRATOR"
      },
      _user: fakeUser,
      app: fakeApp
    };
    await users.updateByUsername(req, res);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(400);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: `Role [${req.body.roleId}] is not supported`
    });
  });
});
