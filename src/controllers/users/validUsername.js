module.exports = async (req, res, next, username) => {
  const { User } = req.app.get("models");
  const user = await User.findOne({ username })
    .lean()
    .exec();

  if (!user) {
    return res.status(404).json({
      message: `Username [${username}] was not exist`
    });
  }
  req._user = user;
  next();
};
