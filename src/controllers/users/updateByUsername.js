module.exports = async (req, res) => {
  let { roleId } = req.body;
  const { _user: user } = req;
  const { User } = req.app.get("models");
  let userProps = {};

  const allowedRoleIds = req.app.get("allowedRoleIds");
  if (roleId && !allowedRoleIds.includes(roleId)) {
    return res.status(400).json({
      message: `Role [${roleId}] is not supported`
    });
  }
  userProps.roleId = roleId;

  let updatedUser = await User.findOneAndUpdate(
    { username: user.username },
    { $set: userProps },
    { new: true }
  )
    .select({ _id: 0, username: 1, updatedAt: 1 })
    .lean()
    .exec();

  res.json({ data: updatedUser });
};
