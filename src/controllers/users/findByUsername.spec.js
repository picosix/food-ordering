const pick = require("lodash/pick");
const faker = require("faker");

const users = require("./index");

const fakeUser = {
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR",
  createdAt: new Date()
};

const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.users.findByUsername", () => {
  beforeEach(() => {
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return user", async () => {
    let req = {
      _user: fakeUser
    };

    await users.findByUsername(req, res);

    expect(res.status).not.toBeCalled();
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: pick(fakeUser, ["username", "name", "roleId"])
    });
  });
});
