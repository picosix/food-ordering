module.exports = async (req, res) => {
  const { _order: order } = req;
  const { Order, Dish } = req.app.get("models");

  const dishes = await Dish.findByIds(req.body.ids);
  if (!dishes.length) {
    return res.json({ data: order });
  }

  const updatedOrder = await Order.findByIdAndUpdate(
    order._id,
    {
      $push: { dishes: { $each: dishes } }
    },
    { new: true }
  )
    .lean()
    .exec();
  return res.json({ data: updatedOrder });
};
