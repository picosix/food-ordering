const moment = require("moment");

module.exports = async (req, res, next) => {
  const { _order: order } = req;
  const { Order } = req.app.get("models");

  if (order.placedAt) {
    return next(new Error(`Order [${order._id}] has been placed yet`));
  }

  const placedAt = req.body.placedAt;
  let updatedOrder = order;
  if (placedAt) {
    updatedOrder = await Order.findByIdAndUpdate(
      order._id,
      { $set: { placedAt: moment().toDate() } },
      { new: true }
    )
      .lean()
      .exec();
  }

  res.json({ data: updatedOrder });
};
