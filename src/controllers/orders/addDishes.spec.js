const faker = require("faker");

const orders = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" ")
};
const fakeOrder = {
  _id: faker.random.uuid(),
  dishes: [fakeDish],
  customerId: faker.random.uuid(),
  createdAt: new Date()
};

const fakeModel = {
  findByIds: jest.fn(() => fakeModel),
  findByIdAndUpdate: jest.fn(() => fakeModel),
  toObject: jest.fn(),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Order: fakeModel, Dish: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.orders.addDishes", () => {
  beforeEach(() => {
    fakeModel.findByIds.mockClear();
    fakeModel.findByIdAndUpdate.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    fakeModel.toObject.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should add found dishes to order", async () => {
    fakeModel.findByIds.mockReturnValueOnce([fakeDish]);
    fakeModel.exec.mockReturnValueOnce({
      ...fakeOrder,
      dishes: [...fakeOrder.dishes, fakeDish]
    });

    let req = {
      body: { ids: [fakeDish._id] },
      _order: fakeOrder,
      app: fakeApp
    };
    await orders.addDishes(req, res);

    expect(fakeModel.findByIds).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIds).toHaveBeenCalledWith([fakeDish._id]);

    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledWith(
      fakeOrder._id,
      {
        $push: { dishes: { $each: [fakeDish] } }
      },
      { new: true }
    );
    expect(fakeModel.lean).toHaveBeenCalledTimes(1);
    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        ...fakeOrder,
        dishes: [...fakeOrder.dishes, fakeDish]
      }
    });
  });

  it("should return found order because we could not found any dishes with provided ids", async () => {
    fakeModel.findByIds.mockReturnValueOnce([]);

    let req = {
      body: { ids: [faker.random.uuid()] },
      _order: fakeOrder,
      app: fakeApp
    };
    await orders.addDishes(req, res);

    expect(fakeModel.findByIds).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIds).toHaveBeenCalledWith(req.body.ids);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ data: fakeOrder });

    expect(fakeModel.findByIdAndUpdate).not.toBeCalled();
    expect(fakeModel.lean).not.toBeCalled();
    expect(fakeModel.exec).not.toBeCalled();
  });
});
