module.exports = async (req, res) => {
  const { Order } = req.app.get("models");
  const { _order: order } = req;

  const deletedOrder = await Order.findByIdAndRemove(order._id)
    .select({ _id: 1, customerId: 1 })
    .lean()
    .exec();

  res.json({ data: deletedOrder });
};
