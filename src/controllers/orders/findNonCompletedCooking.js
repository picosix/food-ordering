module.exports = async (req, res) => {
  const { _paging: paging } = req;
  const { Order } = req.app.get("models");

  let queryFind = {
    placedAt: { $exists: true },
    "dishes.comepletedAt": { $exists: false }
  };

  const sort = { createdAt: -1, ...paging.sort };
  const total = Order.countDocuments(queryFind);
  const orders = await Order.find(queryFind)
    .sort(sort)
    .limit(paging.limit)
    .skip(paging.skip)
    .exec();

  return res.json({ total, data: orders });
};
