const faker = require("faker");

const orders = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};
const fakeOrder = {
  _id: faker.random.uuid(),
  dishes: [fakeDish],
  customerId: faker.random.uuid()
};

const fakeModel = {
  findByIdAndRemove: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Order: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.orders.deleteById", () => {
  beforeEach(() => {
    fakeModel.findByIdAndRemove.mockClear();
    fakeModel.select.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return _id and customerId when deleted order", async () => {
    fakeModel.exec.mockResolvedValueOnce({
      _id: fakeOrder._id,
      customerId: fakeOrder.customerId
    });
    let req = {
      _order: fakeOrder,
      app: fakeApp
    };

    await orders.deleteById(req, res);

    expect(fakeModel.findByIdAndRemove).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIdAndRemove).toHaveBeenCalledWith(fakeOrder._id);

    expect(fakeModel.select).toHaveBeenCalledTimes(1);
    expect(fakeModel.select).toHaveBeenCalledWith({ _id: 1, customerId: 1 });

    expect(fakeModel.lean).toHaveBeenCalledTimes(1);
    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        _id: fakeOrder._id,
        customerId: fakeOrder.customerId
      }
    });
  });
});
