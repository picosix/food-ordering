const mongoose = require("mongoose");
const moment = require("moment");

module.exports = async (req, res) => {
  const { _order: order, params } = req;
  const { Order } = req.app.get("models");
  const completedAt = req.body.completedAt;

  if (!completedAt) {
    return res.json({ data: order });
  }

  const updatedOrder = await Order.findOneAndUpdate(
    {
      _id: mongoose.Types.ObjectId(order._id),
      "dishes._id": mongoose.Types.ObjectId(params.dishId)
    },
    { $set: { "dishes.$.completedAt": moment().toDate() } },
    { new: true }
  )
    .lean()
    .exec();
  return res.json({ data: updatedOrder });
};
