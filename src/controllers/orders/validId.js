module.exports = async (req, res, next, id) => {
  const { Order } = req.app.get("models");
  const order = await Order.findById(id).exec();

  if (!order) {
    return res.status(404).json({
      message: `Order [${id}] was not exist`
    });
  }
  req._order = order;
  next();
};
