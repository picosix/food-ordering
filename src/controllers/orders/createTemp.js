module.exports = async (req, res) => {
  const { _userWithAuthProps: user } = req;
  const { Order } = req.app.get("models");

  let order = await Order.findByCustomerId(user._id);
  res.json({ data: order });
};
