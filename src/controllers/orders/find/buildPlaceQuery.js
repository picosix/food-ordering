module.exports = status => {
  if (typeof status !== "string" || !status) return {};

  if (status === "placed") return { $exists: true };
  return { $exists: false };
};
