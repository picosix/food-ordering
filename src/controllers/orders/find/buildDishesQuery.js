const toLower = require("lodash/toLower");

module.exports = dish => {
  let queryFind = {};
  const { name, description, price, duration, portions } = { ...dish };

  if (name) {
    queryFind.name = new RegExp(toLower(name));
  }
  if (description) {
    queryFind.description = new RegExp(toLower(description));
  }
  if (price) {
    queryFind.price = price;
  }
  if (duration) {
    queryFind.duration = duration;
  }
  if (Array.isArray(portions) && portions.length) {
    queryFind.portions = { $in: portions };
  }

  return queryFind;
};
