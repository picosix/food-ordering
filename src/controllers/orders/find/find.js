const buildDishesQuery = require("./buildDishesQuery");
const buildPlaceQuery = require("./buildPlaceQuery");

module.exports = async function find(req, res) {
  const { _paging: paging, query = {} } = req;
  const { customerId, status, dish } = query;
  const { Order } = req.app.get("models");
  let queryFind = {
    dishes: buildDishesQuery(dish),
    placedAt: buildPlaceQuery(status)
  };

  if (customerId) {
    queryFind.customerId = customerId;
  }

  let total = await Order.countDocuments(queryFind);
  const sort = { createdAt: -1, ...paging.sort };
  let data = await Order.find(queryFind)
    .sort(sort)
    .limit(paging.limit)
    .skip(paging.skip)
    .exec();

  return res.json({ total, data });
};
