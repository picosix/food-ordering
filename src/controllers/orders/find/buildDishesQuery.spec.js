const faker = require("faker");

const buildDishesQuery = require("./buildDishesQuery");

const fakeDish = {
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" ")
};

describe("src.controllers.orders.find.buildDishesQuery", () => {
  it("should return empty object if all of query fields are missed", () => {
    expect(buildDishesQuery()).toEqual({});
  });

  it("should return build query object base on query fields of dish", () => {
    expect(
      buildDishesQuery({
        name: fakeDish.name,
        description: fakeDish.description,
        price: fakeDish.price,
        duration: fakeDish.duration,
        portions: fakeDish.portions
      })
    ).toEqual({
      name: new RegExp(fakeDish.name.toLowerCase()),
      description: new RegExp(fakeDish.description.toLowerCase()),
      price: fakeDish.price,
      duration: fakeDish.duration,
      portions: { $in: fakeDish.portions }
    });
  });
});
