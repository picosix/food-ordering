const range = require("lodash/range");
const faker = require("faker");

const find = require("./find");
const buildDishesQuery = require("./buildDishesQuery");
const buildPlaceQuery = require("./buildPlaceQuery");

jest.mock("./buildDishesQuery");
jest.mock("./buildPlaceQuery");

const fakeTotalOrders = range(1, 201);
const fakeOrders = new Array(fakeTotalOrders).fill({}).map(() => ({
  _id: faker.random.uuid(),
  customerId: faker.random.uuid(),
  dishes: range(1, 11)
    .fill({})
    .map(() => ({
      _id: faker.random.uuid(),
      name: faker.lorem.word(),
      description: faker.lorem.sentence(),
      image: faker.random.image(),
      price: faker.random.number(),
      duration: faker.random.number(),
      portions: faker.lorem.words().split(" ")
    })),
  createdAt: new Date()
}));

const fakeModel = {
  find: jest.fn(() => fakeModel),
  sort: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  populate: jest.fn(() => fakeModel),
  limit: jest.fn(() => fakeModel),
  skip: jest.fn(() => fakeModel),
  countDocuments: jest.fn(),
  exec: jest.fn()
};
const fakeModels = { Order: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.orders.find.find", () => {
  beforeEach(() => {
    buildDishesQuery.mockClear();
    buildPlaceQuery.mockClear();
    fakeModel.find.mockClear();
    fakeModel.sort.mockClear();
    fakeModel.select.mockClear();
    fakeModel.populate.mockClear();
    fakeModel.limit.mockClear();
    fakeModel.skip.mockClear();
    fakeModel.countDocuments.mockClear();
    fakeModel.exec.mockClear();
    fakeApp.get.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return orders on basic query (DEFAULT paging, no filter)", async () => {
    buildDishesQuery.mockReturnValueOnce({});
    buildPlaceQuery.mockReturnValueOnce({});

    let req = {
      _paging: { limit: 20, skip: 0, sort: {} },
      app: fakeApp
    };
    fakeModel.countDocuments.mockReturnValueOnce(fakeTotalOrders);
    fakeModel.exec.mockReturnValueOnce(fakeOrders.slice(0, req._paging.limit));

    await find(req, res);

    expect(buildDishesQuery).toHaveBeenCalledTimes(1);
    expect(buildDishesQuery).toHaveBeenCalledWith(undefined);

    expect(buildPlaceQuery).toHaveBeenCalledTimes(1);
    expect(buildPlaceQuery).toHaveBeenCalledWith(undefined);

    expect(fakeModel.countDocuments).toHaveBeenCalledTimes(1);
    expect(fakeModel.countDocuments).toHaveBeenCalledWith({
      dishes: {},
      placedAt: {}
    });

    expect(fakeModel.find).toHaveBeenCalledTimes(1);
    expect(fakeModel.find).toHaveBeenCalledWith({ dishes: {}, placedAt: {} });

    expect(fakeModel.sort).toHaveBeenCalledTimes(1);
    expect(fakeModel.sort).toHaveBeenCalledWith({
      createdAt: -1,
      ...req._paging.sort
    });

    expect(fakeModel.limit).toHaveBeenCalledTimes(1);
    expect(fakeModel.limit).toHaveBeenCalledWith(req._paging.limit);

    expect(fakeModel.skip).toHaveBeenCalledTimes(1);
    expect(fakeModel.skip).toHaveBeenCalledWith(req._paging.skip);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      total: fakeTotalOrders,
      data: fakeOrders.slice(0, req._paging.limit)
    });
  });

  it("should return orders with filters (customerId, status and dish properties)", async () => {
    const [fakeOrder] = fakeOrders;
    buildDishesQuery.mockReturnValueOnce({
      name: new RegExp(fakeOrder.dishes[0].name.toLowerCase())
    });
    buildPlaceQuery.mockReturnValueOnce({ $exists: false });

    fakeModel.countDocuments.mockReturnValueOnce(1);
    fakeModel.exec.mockReturnValueOnce([fakeOrder]);

    let req = {
      _paging: { limit: 20, skip: 0, sort: { createdAt: -1 } },
      query: {
        customerId: fakeOrder.customerId,
        status: "notPlaced",
        dish: { name: fakeOrder.dishes[0].name }
      },
      app: fakeApp
    };

    await find(req, res);

    const query = {
      customerId: fakeOrder.customerId,
      dishes: {
        name: new RegExp(fakeOrder.dishes[0].name.toLowerCase())
      },
      placedAt: { $exists: false }
    };
    expect(fakeModel.countDocuments).toHaveBeenCalledTimes(1);
    expect(fakeModel.countDocuments).toHaveBeenCalledWith(query);

    expect(fakeModel.find).toHaveBeenCalledTimes(1);
    expect(fakeModel.find).toHaveBeenCalledWith(query);

    expect(fakeModel.sort).toHaveBeenCalledTimes(1);
    expect(fakeModel.sort).toHaveBeenCalledWith({
      createdAt: -1,
      ...req._paging.sort
    });

    expect(fakeModel.limit).toHaveBeenCalledTimes(1);
    expect(fakeModel.limit).toHaveBeenCalledWith(req._paging.limit);

    expect(fakeModel.skip).toHaveBeenCalledTimes(1);
    expect(fakeModel.skip).toHaveBeenCalledWith(req._paging.skip);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      total: 1,
      data: [fakeOrder]
    });
  });
});
