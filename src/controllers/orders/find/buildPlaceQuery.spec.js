const buildPlaceQuery = require("./buildPlaceQuery");

describe("src.controllers.orders.find.buildPlaceQuery", () => {
  it("should return query to find all documents if status is not a truthy string", () => {
    expect(buildPlaceQuery()).toEqual({});
    expect(buildPlaceQuery(null)).toEqual({});
    expect(buildPlaceQuery(true)).toEqual({});
    expect(buildPlaceQuery(1)).toEqual({});
    expect(buildPlaceQuery(NaN)).toEqual({});
  });

  it("should return query to find documents have field placedAt if status is [placed]", () => {
    expect(buildPlaceQuery("placed")).toEqual({ $exists: true });
  });

  it("should return query to find documents haven't field placedAt if status is [notPlaced]", () => {
    expect(buildPlaceQuery("notPlaced")).toEqual({ $exists: false });
  });
});
