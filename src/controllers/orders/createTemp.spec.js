const faker = require("faker");

const orders = require("./index");

const fakeOrder = {
  _id: faker.random.uuid(),
  customerId: faker.random.uuid(),
  dishes: [],
  createdAt: new Date()
};

const fakeModel = {
  findByCustomerId: jest.fn(() => fakeModel),
  toObject: jest.fn()
};
const fakeModels = { Order: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };
const _userWithAuthProps = { _id: faker.random.uuid() };

describe("src.controllers.orders.create", () => {
  beforeEach(() => {
    fakeModel.findByCustomerId.mockClear();
    fakeModel.toObject.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return temporary order", async () => {
    fakeModel.findByCustomerId.mockReturnValueOnce(fakeOrder);

    let req = {
      body: { customerId: fakeOrder.customerId },
      _userWithAuthProps,
      app: fakeApp
    };
    await orders.createTemp(req, res);

    expect(fakeModel.findByCustomerId).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByCustomerId).toHaveBeenCalledWith(
      _userWithAuthProps._id
    );

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeOrder
    });
  });
});
