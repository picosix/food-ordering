module.exports = {
  validId: require("./validId"),
  find: require("./find"),
  getDetails: require("./getDetails"),
  createTemp: require("./createTemp"),
  place: require("./place"),
  deleteById: require("./deleteById"),
  addDishes: require("./addDishes"),
  findNonCompletedCooking: require("./findNonCompletedCooking"),
  markDishAsDone: require("./markDishAsDone")
};
