module.exports = async (req, res) => {
  const { _order: order } = req;

  res.json({ data: order.toObject() });
};
