const faker = require("faker");
const mongoose = require("mongoose");

const orders = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" ")
};
const fakeOrder = {
  _id: faker.random.uuid(),
  dishes: [fakeDish],
  customerId: faker.random.uuid(),
  createdAt: new Date()
};

const fakeModel = {
  findOneAndUpdate: jest.fn(() => fakeModel),
  toObject: jest.fn(),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Order: fakeModel, Dish: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.orders.markDishAsDone", () => {
  beforeAll(() => {
    mongoose.Types.ObjectId = jest.fn(_id => ({ _id }));
  });

  beforeEach(() => {
    fakeModel.findOneAndUpdate.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    fakeModel.toObject.mockClear();
    mongoose.Types.ObjectId.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should mark dish as done of order", async () => {
    const completedAt = new Date();
    fakeModel.exec.mockReturnValueOnce({
      ...fakeOrder,
      dishes: [{ ...fakeDish, completedAt }]
    });

    let req = {
      body: { completedAt },
      _order: fakeOrder,
      params: { dishId: fakeDish._id },
      app: fakeApp
    };
    await orders.markDishAsDone(req, res);

    expect(mongoose.Types.ObjectId).toHaveBeenCalledTimes(2);
    expect(mongoose.Types.ObjectId.mock.calls[0][0]).toBe(fakeOrder._id);
    expect(mongoose.Types.ObjectId.mock.calls[1][0]).toBe(fakeDish._id);

    expect(fakeModel.findOneAndUpdate).toHaveBeenCalledTimes(1);
    expect(fakeModel.findOneAndUpdate).toHaveBeenCalledWith(
      {
        _id: { _id: fakeOrder._id },
        "dishes._id": { _id: fakeDish._id }
      },
      { $set: { "dishes.$.completedAt": expect.any(Date) } },
      { new: true }
    );
    expect(fakeModel.lean).toHaveBeenCalledTimes(1);
    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        ...fakeOrder,
        dishes: [{ ...fakeDish, completedAt }]
      }
    });
  });

  it("should return found order because we could not found flag to set completedAt", async () => {
    let req = {
      body: {},
      _order: fakeOrder,
      params: { dishId: fakeDish._id },
      app: fakeApp
    };
    await orders.markDishAsDone(req, res);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeOrder
    });

    expect(mongoose.Types.ObjectId).not.toBeCalled();
    expect(fakeModel.findOneAndUpdate).not.toBeCalled();
    expect(fakeModel.lean).not.toBeCalled();
    expect(fakeModel.exec).not.toBeCalled();
  });
});
