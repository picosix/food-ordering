const faker = require("faker");
const omit = require("lodash/omit");

const orders = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};
const fakeOrder = {
  _id: faker.random.uuid(),
  placedAt: new Date(),
  dishes: [fakeDish],
  customerId: faker.random.uuid()
};

const fakeModel = {
  findByIdAndUpdate: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Order: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };
const next = jest.fn();

describe("src.controllers.orders.place", () => {
  beforeEach(() => {
    fakeModel.findByIdAndUpdate.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();

    res.json.mockClear();
    res.status.mockClear();
    next.mockClear();
  });

  it("should update status to placed if placedAt was set", async () => {
    fakeModel.exec.mockResolvedValueOnce(fakeOrder);

    let req = {
      body: {
        placedAt: new Date()
      },
      _order: omit(fakeOrder, ["placedAt"]),
      app: fakeApp
    };
    await orders.place(req, res, next);

    expect(next).not.toBeCalled();

    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledWith(
      fakeOrder._id,
      {
        $set: {
          placedAt: expect.any(Date)
        }
      },
      { new: true }
    );
    expect(fakeModel.lean).toHaveBeenCalledTimes(1);
    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeOrder
    });

    expect(next).not.toBeCalled();
  });

  it("should just return found order if placedAt was not set", async () => {
    fakeModel.exec.mockResolvedValueOnce(omit(fakeOrder, ["placedAt"]));

    let req = {
      body: {},
      _order: omit(fakeOrder, ["placedAt"]),
      app: fakeApp
    };
    await orders.place(req, res, next);

    expect(next).not.toBeCalled();

    expect(fakeModel.findByIdAndUpdate).not.toBeCalled();
    expect(fakeModel.lean).not.toBeCalled();
    expect(fakeModel.exec).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: omit(fakeOrder, ["placedAt"])
    });
  });

  it("should move to error handler if place an placed order", async () => {
    let req = {
      body: {},
      _order: fakeOrder,
      app: fakeApp
    };
    await orders.place(req, res, next);

    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith(
      new Error(`Order [${fakeOrder._id}] has been placed yet`)
    );

    expect(fakeModel.findByIdAndUpdate).not.toBeCalled();
    expect(fakeModel.lean).not.toBeCalled();
    expect(fakeModel.exec).not.toBeCalled();
    expect(res.json).not.toBeCalled();
  });
});
