const faker = require("faker");

const dishes = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};
const fakeOrder = {
  _id: faker.random.uuid(),
  dishes: [fakeDish],
  customerId: faker.random.uuid()
};

const fakeModel = {
  findById: jest.fn(() => fakeModel),
  populate: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Order: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };
const next = jest.fn();

describe("src.controllers.orders.validId", () => {
  beforeEach(() => {
    fakeModel.findById.mockClear();
    fakeModel.populate.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    res.json.mockClear();
    res.status.mockClear();
    next.mockClear();
  });

  it("should set req._order by found order", async () => {
    fakeModel.exec.mockReturnValueOnce(fakeOrder);

    let req = { app: fakeApp };
    const _id = fakeOrder._id;
    await dishes.validId(req, res, next, _id);

    expect(fakeModel.findById).toHaveBeenCalledTimes(1);
    expect(fakeModel.findById).toHaveBeenCalledWith(_id);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(req._order).toEqual(fakeOrder);
    expect(next).toHaveBeenCalledTimes(1);
  });

  it("should retrn error because of non exists user", async () => {
    fakeModel.exec.mockReturnValueOnce(null);

    let req = { app: fakeApp };
    const _id = fakeOrder._id;
    await dishes.validId(req, res, next, _id);

    expect(fakeModel.findById).toHaveBeenCalledTimes(1);
    expect(fakeModel.findById).toHaveBeenCalledWith(_id);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(404);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: `Order [${_id}] was not exist`
    });
  });
});
