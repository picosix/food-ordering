const faker = require("faker");

const orders = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};
const fakeOrder = {
  _id: faker.random.uuid(),
  placedAt: new Date(),
  dishes: [fakeDish],
  customerId: faker.random.uuid(),
  toObject: jest.fn(() => fakeOrder)
};

const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.orders.getDetails", () => {
  beforeEach(() => {
    res.json.mockClear();
    res.status.mockClear();
    fakeOrder.toObject.mockClear();
  });

  it("should return found order", async () => {
    let req = {
      _order: fakeOrder
    };

    await orders.getDetails(req, res);

    expect(fakeOrder.toObject).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeOrder
    });
  });
});
