const util = require("util");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const pick = require("lodash/pick");

const signWithPromise = util.promisify(jwt.sign);

module.exports.register = async function register(req, res) {
  const { User } = req.app.get("models");
  let userProps = pick(req.body, [
    "name",
    "username",
    "password",
    "confirmPassword",
    "roleId"
  ]);
  let existingUser = await User.findByUsername(userProps.username);
  if (existingUser) {
    return res.status(400).json({
      message: "Your username has been taken"
    });
  }

  if (!userProps.password || userProps.password !== userProps.confirmPassword) {
    return res.status(400).json({
      message:
        "Password could not be blank and must be matched with confirmed password"
    });
  }
  delete userProps.confirmPassword;
  const { saltRounds } = req.app.get("settings").auth;
  userProps.password = await bcrypt.hash(userProps.password, saltRounds);

  // By default, we set registered user role is customer
  // If we want to change it, tell Administrator to do that
  userProps.roleId = "CUSTOMER";

  let createdUser = await User.create(userProps);

  res.json({ data: { username: createdUser.username } });
};

module.exports.login = async function login(req, res) {
  let { username, password } = req.body;
  const { User } = req.app.get("models");

  let user = await User.findByUsername(username);
  if (!user) {
    return res
      .status(403)
      .json({ message: `Account [${username}] was not found!` });
  }

  let isMatchPassword = await bcrypt.compare(password, user.password);
  if (!isMatchPassword) {
    return res.status(403).json({
      message: "Invalid username or password"
    });
  }

  let jwPpayload = pick(user, ["username", "createdAt"]);
  const { secretKey, expiresIn } = req.app.get("settings").auth;
  let accessToken = await signWithPromise({ payload: jwPpayload }, secretKey, {
    expiresIn
  });

  return res.json({ accessToken });
};

module.exports.me = async function me(req, res) {
  return res.json(req.user && req.user.payload);
};
