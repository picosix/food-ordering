const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const faker = require("faker");

const localController = require("./local");

const rawPassword = "123456";
const fakeUser = {
  username: faker.internet.userName().toLowerCase(),
  password: "$2a$10$Gop7lpKXoP21gxe3mBo0heC4mRYP1VwLW/o7KpZG4rK28pQxUwJjC",
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR",
  createdAt: new Date()
};

jest.mock("bcrypt");
jest.mock("jsonwebtoken");

const fakeModel = {
  findByUsername: jest.fn(),
  create: jest.fn()
};
const settings = { auth: { secretKey: "123", expiresIn: "365d" } };
const fakeModels = { User: fakeModel };
const mapping = {
  settings,
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };
const accessToken = "xyzabc123";
jwt.sign.mockImplementation((jwPpayload, secretKey, opts, cb) => {
  return cb(null, accessToken);
});

describe("src.controllers.auth.local", () => {
  beforeEach(() => {
    fakeModel.findByUsername.mockClear();
    fakeModel.create.mockClear();

    bcrypt.compare.mockClear();
    bcrypt.hash.mockClear();

    fakeApp.get.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  describe("register", () => {
    it("should return registered user", async () => {
      fakeModel.findByUsername.mockResolvedValueOnce(null);
      bcrypt.hash.mockReturnValueOnce(fakeUser.password);
      fakeModel.create.mockReturnValueOnce(fakeUser);

      let req = {
        body: {
          username: fakeUser.username,
          password: rawPassword,
          confirmPassword: rawPassword,
          roleId: fakeUser.roleId
        },
        app: fakeApp
      };
      await localController.register(req, res);

      expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
      expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

      expect(bcrypt.hash).toHaveBeenCalledTimes(1);
      expect(bcrypt.hash).toHaveBeenCalledWith(
        rawPassword,
        settings.auth.saltRounds
      );

      expect(fakeModel.create).toHaveBeenCalledTimes(1);
      expect(fakeModel.create).toHaveBeenCalledWith({
        username: fakeUser.username,
        password: fakeUser.password,
        roleId: "CUSTOMER"
      });

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        data: {
          username: fakeUser.username
        }
      });
    });

    it("should return error because username was exist", async () => {
      fakeModel.findByUsername.mockResolvedValueOnce(fakeUser);

      let req = {
        body: {
          username: fakeUser.username,
          password: rawPassword,
          confirmPassword: rawPassword,
          roleId: fakeUser.roleId
        },
        app: fakeApp
      };
      await localController.register(req, res);

      expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
      expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

      expect(bcrypt.hash).not.toBeCalled();
      expect(fakeModel.create).not.toBeCalled();

      expect(res.status).toHaveBeenCalledTimes(1);
      expect(res.status).toHaveBeenCalledWith(400);

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        message: "Your username has been taken"
      });
    });

    it("should return error because password and confirmPassword is invalid", async () => {
      let req = {
        body: {
          username: fakeUser.username,
          password: rawPassword,
          roleId: fakeUser.roleId
        },
        app: fakeApp
      };
      await localController.register(req, res);

      expect(fakeModel.findByUsername).toHaveBeenCalledTimes(1);
      expect(fakeModel.findByUsername).toHaveBeenCalledWith(fakeUser.username);

      expect(res.status).toHaveBeenCalledTimes(1);
      expect(res.status).toHaveBeenCalledWith(400);

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        message:
          "Password could not be blank and must be matched with confirmed password"
      });
    });
  });

  describe("login", () => {
    it("should return error because of not found account", async () => {
      fakeModel.findByUsername.mockResolvedValueOnce(null);

      let req = {
        body: {
          username: fakeUser.username,
          password: rawPassword
        },
        app: fakeApp
      };

      await localController.login(req, res);

      expect(res.status).toHaveBeenCalledTimes(1);
      expect(res.status).toHaveBeenCalledWith(403);

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        message: `Account [${fakeUser.username}] was not found!`
      });

      expect(bcrypt.compare).not.toBeCalled();
      expect(jwt.sign).not.toBeCalled();
    });

    it("should return error because of invalid password", async () => {
      fakeModel.findByUsername.mockResolvedValueOnce(fakeUser);
      bcrypt.compare.mockResolvedValueOnce(false);

      let password = "abc123";
      let req = {
        body: {
          username: fakeUser.username,
          password
        },
        app: fakeApp
      };

      await localController.login(req, res);
      expect(bcrypt.compare).toHaveBeenCalledTimes(1);
      expect(bcrypt.compare).toHaveBeenCalledWith(password, fakeUser.password);

      expect(res.status).toHaveBeenCalledTimes(1);
      expect(res.status).toHaveBeenCalledWith(403);

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({
        message: "Invalid username or password"
      });

      expect(jwt.sign).not.toBeCalled();
    });

    it("should return accessToken when logged in is successful", async () => {
      fakeModel.findByUsername.mockResolvedValueOnce(fakeUser);
      bcrypt.compare.mockResolvedValueOnce(true);

      let req = {
        body: {
          username: fakeUser.username,
          password: rawPassword
        },
        app: fakeApp
      };

      await localController.login(req, res);

      expect(bcrypt.compare).toHaveBeenCalledTimes(1);
      expect(bcrypt.compare).toHaveBeenCalledWith(
        rawPassword,
        fakeUser.password
      );

      expect(jwt.sign).toHaveBeenCalledTimes(1);
      expect(jwt.sign).toHaveBeenCalledWith(
        {
          payload: {
            username: fakeUser.username,
            createdAt: fakeUser.createdAt
          }
        },
        settings.auth.secretKey,
        { expiresIn: settings.auth.expiresIn },
        expect.any(Function)
      );

      expect(res.status).not.toBeCalled();

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith({ accessToken });
    });
  });

  describe("me", () => {
    it("should return information base on accessToken", async () => {
      let req = {
        user: {
          payload: {
            username: fakeUser.username,
            createdAt: fakeUser.createdAt
          }
        }
      };

      await localController.me(req, res);

      expect(res.status).not.toBeCalled();

      expect(res.json).toHaveBeenCalledTimes(1);
      expect(res.json).toHaveBeenCalledWith(req.user.payload);
    });
  });
});
