module.exports = {
  validId: require("./validId"),
  find: require("./find"),
  findById: require("./findById"),
  create: require("./create"),
  updateById: require("./updateById"),
  deleteById: require("./deleteById")
};
