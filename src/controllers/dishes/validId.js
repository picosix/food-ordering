module.exports = async (req, res, next, id) => {
  const { Dish } = req.app.get("models");
  const dish = await Dish.findById(id)
    .lean()
    .exec();

  if (!dish) {
    return res.status(404).json({
      message: `Dish [${id}] was not exist`
    });
  }
  req._dish = dish;
  next();
};
