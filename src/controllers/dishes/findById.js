module.exports = async (req, res) => {
  const { _dish: dish } = req;

  res.json({ data: dish });
};
