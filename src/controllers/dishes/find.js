const toLower = require("lodash/toLower");

module.exports = async function find(req, res) {
  const { _paging: paging, query = {} } = req;
  const { name, description, price, duration, portions } = query;
  const { Dish } = req.app.get("models");
  let queryFind = {};

  if (name) {
    queryFind.name = new RegExp(toLower(name));
  }
  if (description) {
    queryFind.description = new RegExp(toLower(description));
  }
  if (price) {
    queryFind.price = price;
  }
  if (duration) {
    queryFind.duration = duration;
  }
  if (Array.isArray(portions) && portions.length) {
    queryFind.portions = { $in: portions };
  }

  let total = await Dish.countDocuments(queryFind);
  const sort = { name: 1, ...paging.sort };
  let data = await Dish.find(queryFind)
    .sort(sort)
    .limit(paging.limit)
    .skip(paging.skip)
    .exec();

  return res.json({ total, data });
};
