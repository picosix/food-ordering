const faker = require("faker");

const dishes = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};

const fakeModel = {
  findByIdAndRemove: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Dish: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.dishes.deleteById", () => {
  beforeEach(() => {
    fakeModel.findByIdAndRemove.mockClear();
    fakeModel.select.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return _id and name when deleted dish", async () => {
    fakeModel.exec.mockResolvedValueOnce({
      _id: fakeDish._id,
      name: fakeDish.name
    });
    let req = {
      _dish: fakeDish,
      app: fakeApp
    };

    await dishes.deleteById(req, res);

    expect(fakeModel.findByIdAndRemove).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIdAndRemove).toHaveBeenCalledWith(fakeDish._id);

    expect(fakeModel.select).toHaveBeenCalledTimes(1);
    expect(fakeModel.select).toHaveBeenCalledWith({ _id: 1, name: 1 });

    expect(fakeModel.lean).toHaveBeenCalledTimes(1);
    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        _id: fakeDish._id,
        name: fakeDish.name
      }
    });
  });
});
