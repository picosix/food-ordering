module.exports = async (req, res) => {
  const { Dish } = req.app.get("models");
  const { _dish: dish } = req;

  const deletedDish = await Dish.findByIdAndRemove(dish._id)
    .select({ _id: 1, name: 1 })
    .lean()
    .exec();

  res.json({ data: deletedDish });
};
