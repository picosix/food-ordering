const faker = require("faker");

const dishes = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};

const fakeModel = {
  findByIdAndUpdate: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Dish: fakeModel };
const mapping = {
  models: fakeModels,
  allowedRoleIds: ["ADMINISTRATOR", "CUSTOMER", "CHEF"]
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.dishes.updateById", () => {
  beforeEach(() => {
    fakeModel.findByIdAndUpdate.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should update dish successful", async () => {
    fakeModel.exec.mockResolvedValueOnce(fakeDish);

    let req = {
      body: {
        name: fakeDish.name,
        portions: fakeDish.portions
      },
      _dish: {
        ...fakeDish,
        name: faker.random.word().toLowerCase(),
        portions: faker.lorem.words().split(" ")
      },
      app: fakeApp
    };
    await dishes.updateById(req, res);

    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledWith(
      fakeDish._id,
      {
        $set: {
          name: fakeDish.name,
          portions: fakeDish.portions
        }
      },
      { new: true }
    );
    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeDish
    });
  });

  it("should allow update dish's portions by string", async () => {
    fakeModel.exec.mockResolvedValueOnce({
      ...fakeDish,
      portions: fakeDish.portions[0]
    });

    let req = {
      body: {
        portions: fakeDish.portions[0]
      },
      _dish: fakeDish,
      app: fakeApp
    };
    await dishes.updateById(req, res);

    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledTimes(1);
    expect(fakeModel.findByIdAndUpdate).toHaveBeenCalledWith(
      fakeDish._id,
      {
        $set: {
          portions: [fakeDish.portions[0]]
        }
      },
      { new: true }
    );
    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: {
        ...fakeDish,
        portions: fakeDish.portions[0]
      }
    });
  });
});
