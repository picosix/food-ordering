const faker = require("faker");
const omit = require("lodash/omit");

const dishes = require("./index");

const fakeDish = {
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};

const fakeModel = {
  create: jest.fn(() => fakeModel),
  toObject: jest.fn()
};
const fakeModels = { Dish: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.dishes.create", () => {
  beforeEach(() => {
    fakeModel.create.mockClear();
    fakeModel.toObject.mockClear();

    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return created dish", async () => {
    fakeModel.toObject.mockReturnValueOnce(fakeDish);

    let req = {
      body: omit(fakeDish, ["createdAt", "updatedAt"]),
      app: fakeApp
    };
    await dishes.create(req, res);

    expect(fakeModel.create).toHaveBeenCalledTimes(1);
    expect(fakeModel.create).toHaveBeenCalledWith(
      omit(fakeDish, ["createdAt", "updatedAt"])
    );

    expect(fakeModel.toObject).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeDish
    });
  });

  it("should allow create dish with ONE portion", async () => {
    const portions = faker.random.word();
    fakeModel.toObject.mockReturnValueOnce({
      ...fakeDish,
      portions: [portions]
    });

    let req = {
      body: { ...omit(fakeDish, ["createdAt", "updatedAt"]), portions },
      app: fakeApp
    };
    await dishes.create(req, res);

    expect(fakeModel.create).toHaveBeenCalledTimes(1);
    expect(fakeModel.create).toHaveBeenCalledWith({
      ...omit(fakeDish, ["createdAt", "updatedAt"]),
      portions: [portions]
    });

    expect(fakeModel.toObject).toHaveBeenCalledTimes(1);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: { ...fakeDish, portions: [portions] }
    });
  });
});
