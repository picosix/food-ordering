const faker = require("faker");

const dishes = require("./index");

const fakeDish = {
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};

const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.dishes.findById", () => {
  beforeEach(() => {
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return found dish", async () => {
    let req = {
      _dish: fakeDish
    };

    await dishes.findById(req, res);

    expect(res.status).not.toBeCalled();
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      data: fakeDish
    });
  });
});
