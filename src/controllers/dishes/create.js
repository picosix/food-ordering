const pick = require("lodash/pick");

module.exports = async (req, res) => {
  const { Dish } = req.app.get("models");

  let dishProps = pick(req.body, [
    "name",
    "description",
    "image",
    "price",
    "duration",
    "portions"
  ]);

  if (typeof dishProps.portions === "string" && dishProps.portions) {
    dishProps.portions = [dishProps.portions];
  }
  let createdDish = await Dish.create(dishProps);

  res.json({ data: createdDish.toObject() });
};
