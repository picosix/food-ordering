const pick = require("lodash/pick");

module.exports = async (req, res) => {
  const { _dish: dish } = req;
  const { Dish } = req.app.get("models");
  let dishProps = pick(req.body, [
    "name",
    "description",
    "image",
    "price",
    "duration",
    "portions"
  ]);

  if (typeof dishProps.portions === "string" && dishProps.portions) {
    dishProps.portions = [dishProps.portions];
  }

  let updatedDish = await Dish.findByIdAndUpdate(
    dish._id,
    { $set: dishProps },
    { new: true }
  )
    .lean()
    .exec();

  res.json({ data: updatedDish });
};
