const faker = require("faker");

const dishes = require("./index");

const fakeDish = {
  _id: faker.random.uuid(),
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
};

const fakeModel = {
  findById: jest.fn(() => fakeModel),
  lean: jest.fn(() => fakeModel),
  exec: jest.fn(() => fakeModel)
};
const fakeModels = { Dish: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };
const next = jest.fn();

describe("src.controllers.dishes.validId", () => {
  beforeEach(() => {
    fakeModel.findById.mockClear();
    fakeModel.lean.mockClear();
    fakeModel.exec.mockClear();
    res.json.mockClear();
    res.status.mockClear();
    next.mockClear();
  });

  it("should set req._dish by found dish", async () => {
    fakeModel.exec.mockReturnValueOnce(fakeDish);

    let req = { app: fakeApp };
    const _id = fakeDish._id;
    await dishes.validId(req, res, next, _id);

    expect(fakeModel.findById).toHaveBeenCalledTimes(1);
    expect(fakeModel.findById).toHaveBeenCalledWith(_id);

    expect(req._dish).toEqual(fakeDish);
    expect(next).toHaveBeenCalledTimes(1);
  });

  it("should retrn error because of non exists dish", async () => {
    fakeModel.exec.mockReturnValueOnce(null);

    let req = { app: fakeApp };
    const _id = fakeDish._id;
    await dishes.validId(req, res, next, _id);

    expect(fakeModel.findById).toHaveBeenCalledTimes(1);
    expect(fakeModel.findById).toHaveBeenCalledWith(_id);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(404);

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: `Dish [${_id}] was not exist`
    });
  });
});
