const range = require("lodash/range");
const faker = require("faker");

const dishes = require("./index");

const fakeTotalDishes = range(1, 201);
const fakeDishes = new Array(fakeTotalDishes).fill({}).map(() => ({
  name: faker.lorem.word(),
  description: faker.lorem.sentence(),
  image: faker.random.image(),
  price: faker.random.number(),
  duration: faker.random.number(),
  portions: faker.lorem.words().split(" "),
  createdAt: new Date()
}));

const fakeModel = {
  find: jest.fn(() => fakeModel),
  sort: jest.fn(() => fakeModel),
  select: jest.fn(() => fakeModel),
  populate: jest.fn(() => fakeModel),
  limit: jest.fn(() => fakeModel),
  skip: jest.fn(() => fakeModel),
  countDocuments: jest.fn(),
  exec: jest.fn()
};
const fakeModels = { Dish: fakeModel };
const mapping = {
  models: fakeModels
};
const fakeApp = {
  get: jest.fn(key => mapping[key])
};
const res = { json: jest.fn(), status: jest.fn(() => res) };

describe("src.controllers.dishes.find", () => {
  beforeEach(() => {
    fakeModel.find.mockClear();
    fakeModel.sort.mockClear();
    fakeModel.select.mockClear();
    fakeModel.populate.mockClear();
    fakeModel.limit.mockClear();
    fakeModel.skip.mockClear();
    fakeModel.countDocuments.mockClear();
    fakeModel.exec.mockClear();
    fakeApp.get.mockClear();
    res.json.mockClear();
    res.status.mockClear();
  });

  it("should return dishes on basic query (DEFAULT paging, no filter)", async () => {
    let req = {
      _paging: { limit: 20, skip: 0, sort: {} },
      app: fakeApp
    };
    fakeModel.countDocuments.mockReturnValueOnce(fakeTotalDishes);
    fakeModel.exec.mockReturnValueOnce(fakeDishes.slice(0, req._paging.limit));

    await dishes.find(req, res);

    expect(fakeModel.countDocuments).toHaveBeenCalledTimes(1);
    expect(fakeModel.countDocuments).toHaveBeenCalledWith({});

    expect(fakeModel.find).toHaveBeenCalledTimes(1);
    expect(fakeModel.find).toHaveBeenCalledWith({});

    expect(fakeModel.sort).toHaveBeenCalledTimes(1);
    expect(fakeModel.sort).toHaveBeenCalledWith({
      name: 1,
      ...req._paging.sort
    });

    expect(fakeModel.limit).toHaveBeenCalledTimes(1);
    expect(fakeModel.limit).toHaveBeenCalledWith(req._paging.limit);

    expect(fakeModel.skip).toHaveBeenCalledTimes(1);
    expect(fakeModel.skip).toHaveBeenCalledWith(req._paging.skip);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      total: fakeTotalDishes,
      data: fakeDishes.slice(0, req._paging.limit)
    });
  });

  it("should return dishes with filters (name, description, price, duration and portions)", async () => {
    const [fakeDish] = fakeDishes;
    fakeModel.countDocuments.mockReturnValueOnce(1);
    fakeModel.exec.mockReturnValueOnce([fakeDish]);

    let req = {
      _paging: { limit: 20, skip: 0, sort: { createdAt: -1 } },
      query: {
        name: fakeDish.name,
        description: fakeDish.description,
        price: fakeDish.price,
        duration: fakeDish.duration,
        portions: fakeDish.portions
      },
      app: fakeApp
    };

    await dishes.find(req, res);

    const query = {
      name: new RegExp(fakeDish.name.toLowerCase()),
      description: new RegExp(fakeDish.description.toLowerCase()),
      price: fakeDish.price,
      duration: fakeDish.duration,
      portions: { $in: fakeDish.portions }
    };
    expect(fakeModel.countDocuments).toHaveBeenCalledTimes(1);
    expect(fakeModel.countDocuments).toHaveBeenCalledWith(query);

    expect(fakeModel.find).toHaveBeenCalledTimes(1);
    expect(fakeModel.find).toHaveBeenCalledWith(query);

    expect(fakeModel.sort).toHaveBeenCalledTimes(1);
    expect(fakeModel.sort).toHaveBeenCalledWith({
      name: 1,
      ...req._paging.sort
    });

    expect(fakeModel.limit).toHaveBeenCalledTimes(1);
    expect(fakeModel.limit).toHaveBeenCalledWith(req._paging.limit);

    expect(fakeModel.skip).toHaveBeenCalledTimes(1);
    expect(fakeModel.skip).toHaveBeenCalledWith(req._paging.skip);

    expect(fakeModel.exec).toHaveBeenCalledTimes(1);

    expect(res.status).not.toBeCalled();

    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      total: 1,
      data: [fakeDish]
    });
  });
});
