const mongoose = require("mongoose");

const DishSchema = require("./Dish.schema");

const schema = new mongoose.Schema(
  {
    placedAt: Date,
    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    dishes: [DishSchema.schema]
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

schema.virtual("details").get(function() {
  return this.dishes.reduce(
    (details, dish) => ({
      price: details.price + dish.price,
      duration: details.duration + dish.duration,
      waitingTime: details.waitingTime + (dish.completedAt ? 0 : dish.duration)
    }),
    {
      price: 0,
      duration: 0,
      waitingTime: 0
    }
  );
});

schema.statics.findByCustomerId = async function findByCustomerId(customerId) {
  const oldOrder = await this.findOne({
    customerId: mongoose.Types.ObjectId(customerId),
    placedAt: { $exists: false }
  }).exec();
  if (oldOrder) return oldOrder.toObject();

  const newOrder = await this.create({ customerId, dishes: [] });
  return newOrder.toObject();
};

module.exports = { name: "Order", schema };
