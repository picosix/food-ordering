const connectMongo = require("../packages/mongoose");

const UserSchema = require("./User.schema");
const OrderSchema = require("./Order.schema");
const DishSchema = require("./Dish.schema");

module.exports = connection =>
  connectMongo({
    connection,
    schemas: [UserSchema, OrderSchema, DishSchema]
  });
