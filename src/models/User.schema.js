const mongoose = require("mongoose");
const toLowser = require("lodash/toLower");

const schema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      validate: {
        validator: function(username) {
          return /[_\d+\w+]+/.test(username);
        },
        message: () =>
          "This allows just alphanumeric charactersand the underscore"
      }
    },
    password: { type: String, required: true, minlength: 6 },
    name: { first: String, last: String, middle: String },
    roleId: { type: String }
  },
  { timestamps: true }
);

schema.virtual("fullNanme").get(function() {
  const { first, last, middle } = this.name;

  return [first, middle, last].filter(name => name).join(" ");
});

schema.statics.findByUsername = async function findByUsername(username) {
  username = toLowser(username);
  return this.findOne(
    { username },
    {
      username: 1,
      password: 1
    }
  )
    .lean()
    .exec();
};

schema.statics.getAuthProps = async function getAuthProps({ username }) {
  username = toLowser(username);
  return this.findOne({ username })
    .select(["username", "roleId"])
    .lean()
    .exec();
};

module.exports = { name: "User", schema };
