const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    description: String,
    image: String,
    price: { type: Number, min: 0 },
    duration: { type: Number, min: 0 },
    portions: [String],
    cookingBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    completedAt: Date
  },
  { timestamps: true }
);

schema.statics.findByIds = async function findByIds(ids) {
  if (!Array.isArray(ids) || !ids.length) {
    return [];
  }

  return this.find({
    _id: { $in: ids.map(_id => mongoose.Types.ObjectId(_id)) }
  })
    .select({
      _id: 0,
      name: 1,
      description: 1,
      image: 1,
      price: 1,
      duration: 1,
      portions: 1
    })
    .lean()
    .exec();
};

module.exports = { name: "Dish", schema };
