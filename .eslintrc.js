module.exports = {
  extends: ["standard", "plugin:prettier/recommended"],
  plugins: ["jest"],
  env: {
    "jest/globals": true,
    node: true
  }
};
