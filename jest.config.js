module.exports = {
  verbose: process.env.NODE_ENV === "test",
  setupTestFrameworkScriptFile: "<rootDir>/jest.setup.js",
  testEnvironment: "node"
};
