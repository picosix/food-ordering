require("dotenv").config();

const server = require("../src/server");
const config = require("../config");

// Custom middlewares
const authentication = require("../src/middleware/authentication");
const authorization = require("../src/middleware/authorization");
const errorHandler = require("../src/middleware/errorHandler");

// Routes
const authRouter = require("../src/routes/auth");
const userRouter = require("../src/routes/users");
const orderRouter = require("../src/routes/orders");
const dishRouter = require("../src/routes/dishes");
const rootRouter = require("../src/routes/root");
const notFoundRoute = require("../src/routes/notFound");

// Custom middlewares
server.use(
  authentication({
    redis: config.database.redis,
    secretKey: config.auth.secretKey,
    publicPaths: config.auth.publicPaths
  })
);
server.use(
  authorization({
    grantAccess: config.auth.grantAccess,
    permissionsMapping: config.auth.permissionsMapping,
    publicPaths: config.auth.publicPaths
  })
);

// Routes
server.use("/auth", authRouter);
server.use("/users", userRouter);
server.use("/orders", orderRouter);
server.use("/dishes", dishRouter);
server.use("/", rootRouter);
server.use(notFoundRoute);

server.use(errorHandler());

let port = server.get("settings").port;
let logger = server.get("logger");

server.listen(port, () => logger.info(`API run at http://localhost:${port}`));
