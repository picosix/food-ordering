module.exports = {
  maxLimit: Number(process.env.API_QUERY_MAX_LIMIT) || 100,
  defaultLimit: Number(process.env.API_QUERY_DEFAULT_LIMIT) || 20,
  maxPage: Number(process.env.API_QUERY_MAX_PAGE) || 10
};
