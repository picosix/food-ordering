module.exports = {
  mongo: {
    host: process.env.DEFAULT_MONGO_HOST || "localhost",
    port: Number(process.env.DEFAULT_MONGO_PORT) || 27017,
    dbName: process.env.DEFAULT_MONGO_DATABASE || "api",
    options: {
      user: process.env.DEFAULT_MONGO_USERNAME,
      pass: process.env.DEFAULT_MONGO_PASSWORD
    }
  },
  redis: {
    host: process.env.DEFAULT_REDIS_HOST || "localhost",
    port: Number(process.env.DEFAULT_REDIS_PORT) || 6379,
    db: Number(process.env.DEFAULT_REDIS_DATABSE) || 1
  }
};
