const auth = require("./auth");
const restQueryParser = require("./restQueryParser");
const database = require("./database");

module.exports = {
  debug: !["production", "prod"].includes(process.env.NODE_ENV),
  port: Number(process.env.API_PORT) || 9001,
  auth,
  restQueryParser,
  database
};
