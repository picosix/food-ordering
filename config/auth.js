module.exports = {
  secretKey: process.env.AUTH_SECRET_KEY,
  expiresIn: process.env.AUTH_SECRET_EXPIRES_IN || "365d",
  saltRounds: Number(process.env.AUTH_SALT_ROUNDS) || 10,
  publicPaths: [
    { url: "/auth/local/register", method: "POST" },
    { url: "/auth/local", method: "POST" }
  ],
  grantAccess: [
    {
      roles: ["ADMINISTRATOR"],
      allows: [
        {
          resources: ["users", "auth", "orders", "dishes"],
          permissions: "*"
        }
      ]
    },
    {
      roles: ["CUSTOMER"],
      allows: [
        { resources: "auth", permissions: ["register", "login"] },
        {
          resources: "orders",
          permissions: ["createTemp", "getDetails", "addDishes", "place"]
        },
        { resources: "dishes", permissions: ["find", "findById"] }
      ]
    },
    {
      roles: ["CHEF"],
      allows: [
        {
          resources: "orders",
          permissions: ["findNonCompletedCooking", "markDishAsDone"]
        }
      ]
    }
  ],
  permissionsMapping: [
    {
      resourcesName: "users",
      actionName: "findByUsername",
      url: /\/users\/[_\d+\w+]+\/?/,
      method: "GET"
    },
    {
      resourcesName: "users",
      actionName: "updateByUsername",
      url: /\/users\/[_\d+\w+]+\/?/,
      method: "PATCH"
    },
    {
      resourcesName: "users",
      actionName: "deleteByUsername",
      url: /\/users\/[_\d+\w+]+\/?/,
      method: "DELETE"
    },
    {
      resourcesName: "users",
      actionName: "create",
      url: /\/users\/?/,
      method: "POST"
    },
    {
      resourcesName: "users",
      actionName: "find",
      url: /\/users\/?/,
      method: "GET"
    },
    {
      resourcesName: "orders",
      actionName: "createTemp",
      url: /\/orders\/temp\/?/,
      method: "POST"
    },
    {
      resourcesName: "orders",
      actionName: "markDishAsDone",
      url: /\/orders\/[_\d+\w+]+\/dishes\/[_\d+\w+]+\/?/,
      method: "PATCH"
    },
    {
      resourcesName: "orders",
      actionName: "addDishes",
      url: /\/orders\/[_\d+\w+]+\/dishes\/?/,
      method: "POST"
    },
    {
      resourcesName: "orders",
      actionName: "findNonCompletedCooking",
      url: /\/orders\/[_\d+\w+]+\/dishes\/?/,
      method: "GET"
    },
    {
      resourcesName: "orders",
      actionName: "getDetails",
      url: /\/orders\/[_\d+\w+]+\/?/,
      method: "GET"
    },
    {
      resourcesName: "orders",
      actionName: "place",
      url: /\/orders\/[_\d+\w+]+\/?/,
      method: "PATCH"
    },
    {
      resourcesName: "orders",
      actionName: "deleteById",
      url: /\/orders\/[_\d+\w+]+\/?/,
      method: "DELETE"
    },
    {
      resourcesName: "orders",
      actionName: "find",
      url: /\/orders\/?/,
      method: "GET"
    },
    {
      resourcesName: "dishes",
      actionName: "findById",
      url: /\/dishes\/[_\d+\w+]+\/?/,
      method: "GET"
    },
    {
      resourcesName: "dishes",
      actionName: "updateById",
      url: /\/dishes\/[_\d+\w+]+\/?/,
      method: "PATCH"
    },
    {
      resourcesName: "dishes",
      actionName: "deleteById",
      url: /\/dishes\/[_\d+\w+]+\/?/,
      method: "DELETE"
    },
    {
      resourcesName: "dishes",
      actionName: "create",
      url: /\/dishes\/?/,
      method: "POST"
    },
    {
      resourcesName: "dishes",
      actionName: "find",
      url: /\/dishes\/?/,
      method: "GET"
    }
  ]
};
