require("dotenv").config();
jest.setTimeout(50000);

let env = process.env.NODE_ENV;
if (env !== "test") {
  let message = `Current env: ${env}. Make sure you run test on test environment`;
  throw new Error(message);
}
