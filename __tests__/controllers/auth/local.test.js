const request = require("supertest");
const faker = require("faker");
const jwt = require("jsonwebtoken");

const server = require("../../../src/server");
const config = require("../../../config");

// Custom middlewares
const authentication = require("../../../src/middleware/authentication");
const authorization = require("../../../src/middleware/authorization");
const errorHandler = require("../../../src/middleware/errorHandler");

// Routes
const authRouter = require("../../../src/routes/auth");
const notFoundRoute = require("../../../src/routes/notFound");

const password = "123456";
const registerProps = {
  name: { first: faker.name.firstName(), last: faker.name.lastName() },
  username: faker.internet.userName(),
  password,
  confirmPassword: password,
  roleId: "ADMINISTRATOR"
};

describe("controllers.auth.local", () => {
  beforeAll(async () => {
    // Custom middlewares
    server.use(
      authentication({
        redis: config.database.redis,
        secretKey: config.auth.secretKey,
        publicPaths: config.auth.publicPaths
      })
    );
    server.use(
      authorization({
        grantAccess: config.auth.grantAccess,
        permissionsMapping: config.auth.permissionsMapping,
        publicPaths: config.auth.publicPaths
      })
    );

    // Routes
    server.use("/auth", authRouter);
    server.use(notFoundRoute);
    server.use(errorHandler);

    await server.get("models").User.deleteMany();
  });

  describe("POST /auth/local/register", () => {
    it("should return registered account informations", async () => {
      const { body } = await request(server)
        .post("/auth/local/register")
        .send(registerProps)
        .set("Accept", "application/json")
        .expect(200);

      const { data: registeredAccount } = body;
      expect(registeredAccount.username).toBe(
        registerProps.username.toLowerCase()
      );
    });
  });

  describe("POST /auth/local", () => {
    it("should return registered account informations", async () => {
      const { body } = await request(server)
        .post("/auth/local")
        .send({ username: registerProps.username, password })
        .set("Accept", "application/json")
        .expect(200);

      expect(body.accessToken).toBeTruthy();
      const userPropsInToken = jwt.decode(body.accessToken);
      expect(userPropsInToken.payload).toBeTruthy();
      expect(userPropsInToken.payload.username).toBe(
        registerProps.username.toLowerCase()
      );
    });
  });
});
