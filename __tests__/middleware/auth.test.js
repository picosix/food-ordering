const util = require("util");
const request = require("supertest");
const faker = require("faker");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Redis = require("ioredis");

const server = require("../../src/server");
const config = require("../../config");

// Custom middlewares
const authentication = require("../../src/middleware/authentication");
const authorization = require("../../src/middleware/authorization");
const errorHandler = require("../../src/middleware/errorHandler");

// Routes
const authRouter = require("../../src/routes/auth");
const userRouter = require("../../src/routes/users");
const notFoundRoute = require("../../src/routes/notFound");

const signWithPromise = util.promisify(jwt.sign);

const password = "123456";
const adminUser = {
  username: faker.internet.userName().toLowerCase(),
  password: bcrypt.hashSync(password, config.auth.saltRounds),
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "ADMINISTRATOR"
};
const customerUser = {
  username: faker.internet.userName().toLowerCase(),
  password: bcrypt.hashSync(password, config.auth.saltRounds),
  name: {
    first: faker.name.firstName(),
    last: faker.name.lastName(),
    middle: faker.name.findName()
  },
  roleId: "CUSTOMER"
};

const redisClient = new Redis(config.database.redis);

describe("middleware.auth*", () => {
  let adminToken;
  let customerToken;

  beforeAll(async () => {
    // Custom middlewares
    server.use(
      authentication({
        redis: config.database.redis,
        secretKey: config.auth.secretKey,
        publicPaths: config.auth.publicPaths
      })
    );
    server.use(
      authorization({
        grantAccess: config.auth.grantAccess,
        permissionsMapping: config.auth.permissionsMapping,
        publicPaths: config.auth.publicPaths
      })
    );

    // Routes
    server.use("/auth", authRouter);
    server.use("/users", userRouter);
    server.use(notFoundRoute);
    server.use(errorHandler);

    await redisClient.flushall();
    await server.get("models").User.deleteMany();
    await server.get("models").User.insertMany([adminUser, customerUser]);
    adminToken = await signWithPromise(
      { payload: { username: adminUser.username.toLowerCase() } },
      config.auth.secretKey,
      {
        expiresIn: config.auth.expiresIn
      }
    );
    customerToken = await signWithPromise(
      { payload: { username: customerUser.username.toLowerCase() } },
      config.auth.secretKey,
      {
        expiresIn: config.auth.expiresIn
      }
    );
  });

  it("should allow ADMINISTRATOR get list users", async () => {
    const { body } = await request(server)
      .get("/users")
      .set("Accept", "application/json")
      .set("Authorization", `Bearer ${adminToken}`)
      .expect(200);
    const { total, data } = body;

    expect(total).toBe(2);
    expect(Array.isArray(data)).toBeTruthy();
    expect(data.length).toBe(2);
  });

  it("should return error if CUSTOMER request API of ADMINISTRATOR", async () => {
    const response = await request(server)
      .get("/users")
      .set("Accept", "application/json")
      .set("Authorization", `Bearer ${customerToken}`)
      .expect(403);

    expect(response.body.message).toBeTruthy();
    expect(response.body.message).toEqual(expect.stringContaining("Forbidden"));
  });
});
